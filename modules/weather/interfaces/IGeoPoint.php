<?php

namespace app\modules\weather\interfaces;


interface IGeoPoint
{
    public function getLat();

    public function getLon();

    public function getId();
}