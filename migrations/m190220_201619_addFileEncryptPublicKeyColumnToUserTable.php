<?php

use yii\db\Migration;


class m190220_201619_addFileEncryptPublicKeyColumnToUserTable extends Migration
{

    public function safeUp()
    {
        $this->addColumn(\app\models\db\User::tableName(), 'file_encode_public_key', $this->text()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn(\app\models\db\User::tableName(), 'file_encode_public_key');
    }
}
