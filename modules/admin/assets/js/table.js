$(function () {
    $('.table-submit').click(function () {
        $('.table-form').attr('action', '/admin/order/table');
        $('.table-form').attr('method', 'get');
        $('.table-form').submit();
    });

    $('.table-to-excel').click(function () {
        $('.table-form').attr('action', '/admin/order/print');
        $('.table-form').attr('method', 'post');
        $('.table-form').submit();
    });

    $('.table-print').click(function () {
        $('.table-form').attr('action', '/admin/order/print-table');
        $('.table-form').attr('target', '_blank');
        $('.table-form').attr('method', 'post');
        $('.table-form').submit();
    });
});