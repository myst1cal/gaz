<?php

namespace app\models\forms;

use app\models\db\User;
use app\repositories\UserRepository;
use Yii;
use yii\base\Model;


/**
 *
 * @property null|\app\models\db\User $user
 */
class LoginForm extends Model
{
    public $xcode;
    public $password;
    public $rememberMe = true;

    private $_user;


    public function rules()
    {
        return [
            [['xcode', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'xcode' => 'X - код',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня'
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный xcode или пароль.');
            }
        }
    }

    /**
     * Logs in a user using the provided xcode and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = UserRepository::findByXcode($this->xcode);
        }

        return $this->_user;
    }
}
