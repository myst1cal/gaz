<?php

namespace app\repositories;


use app\models\db\User;

class UserRepository
{
    /**
     * Find user by xcode
     *
     * @param string $xcode
     * @return User
     */
    public static function findByXcode($xcode)
    {
        return User::findOne(['xcode' => $xcode, 'deleted' => User::NOT_DELETED]);
    }
}