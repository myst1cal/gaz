<?php

use yii\db\Migration;

class m190220_202445_updateFileTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn(\app\models\db\File::tableName(), 'file_name_encode', $this->string()->null());
        $this->addColumn(\app\models\db\File::tableName(), 'encode_public_key', $this->text()->null());
    }

    public function safeDown()
    {
        $this->dropColumn(\app\models\db\File::tableName(), 'file_name_encode');
        $this->dropColumn(\app\models\db\File::tableName(), 'encode_public_key');
    }
}
