<?php

namespace app\repositories;


use app\dto\phone\PhoneDto;
use app\models\db\Phone;

class PhoneRepository
{

    public static function save(PhoneDto $phoneDto)
    {
        if ($phoneDto->id) {
            $phone = Phone::findOne($phoneDto->id);
        } else {
            $phone = new Phone();
        }

        $phone->user_id = $phoneDto->userId;
        $phone->number = $phoneDto->number;
        $phone->comment = $phoneDto->comment;

        if ($phone->save()) {
            return $phone;
        }

        throw new \Exception(implode(';', $phone->getErrors()));
    }

    public static function clearByUserId(int $userId)
    {
        Phone::deleteAll(['user_id' => $userId]);
    }


}