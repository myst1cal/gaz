<?php
/** @var \app\models\db\OrderHistory $orderHistory */
?>


<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>Время</th>
        <th>Год/месяц</th>
        <th>Z-код</th>
        <?php foreach (range(1, 31) as $day): ?>
            <th><?= $day ?></th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= date('Y-m-d h:i:s', $orderHistory->date_time) ?></td>
        <td><?= $orderHistory->year . '/' . $orderHistory->month ?></td>
        <td><?= $orderHistory->meter->zcode ?></td>
        <?php foreach (range(1, 31) as $day): ?>
            <td><?= $orderHistory->{'day_' . $day} ?></td>
        <?php endforeach; ?>
    </tr>
    </tbody>
</table>