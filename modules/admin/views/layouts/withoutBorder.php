<?php

/* @var $this View */

/* @var $content string */

use app\modules\admin\assets\AppAsset;
use app\modules\admin\services\NoticeService;
use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Юг-газ',
        'brandUrl'   => Yii::$app->homeUrl,
        'options'    => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Пользователь', 'url' => ['/admin/user/info']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
    } else {
        $menuItems[] = ['label' => NoticeService::hasNotice() . 'Уведомления', 'url' => ['/admin/notice/index']];
        $menuItems[] = ['label' => 'Файлы', 'url' => ['/admin/file/index']];
        $menuItems[] = ['label' => 'Ж/Д', 'url' => ['/admin/railway/index']];
        $menuItems[] = ['label' => 'Пользователи', 'url' => ['/admin/user/index']];
//        $menuItems[] = ['label' => 'Заказы', 'url' => ['/admin/order/index']];
        $menuItems[] = ['label' => 'Города', 'url' => ['/admin/city/index']];
        $menuItems[] = ['label' => 'Операторы ГТС/ГРМ', 'url' => ['/admin/provider/index']];
        $menuItems[] = [
            'label'   => 'Отчеты',
            'options' => ['id' => 'down_history'],
            'items'   => [
                ['label' => 'Месячный', 'url' => ['/admin/report/month'], 'options' => ['id' => 'wn_history']],
                ['label' => 'Дневной', 'url' => ['/admin/report/day'], 'options' => ['id' => 'wn_history']],
            ],
        ];
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выход (' . Yii::$app->user->identity->xcode . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'encodeLabels' => false,
        'options'      => ['class' => 'navbar-nav navbar-right'],
        'items'        => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container1200">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
