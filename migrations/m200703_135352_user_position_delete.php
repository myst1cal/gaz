<?php

use app\models\db\User;
use yii\db\Migration;

class m200703_135352_user_position_delete extends Migration
{
    public function safeUp()
    {
        $this->dropColumn(User::tableName(), 'position1');
        $this->dropColumn(User::tableName(), 'position2');

        $this->addColumn(User::tableName(), 'signatory', $this->smallInteger()->defaultValue(User::SIGNATORY_DIRECTOR)->comment('Подписант'));
    }

    public function safeDown()
    {
    }
}
