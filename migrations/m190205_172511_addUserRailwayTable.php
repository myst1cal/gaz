<?php

use app\models\db\User;
use yii\db\Migration;

/**
 * Class m190205_172511_addUserRailwayTable
 */
class m190205_172511_addUserRailwayTable extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_user_railway_id', User::tableName());

        $this->dropIndex('user_railway_id', User::tableName());

        $this->dropColumn(User::tableName(), 'railway_id');

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable(
            '{{%user_railway}}',
            [
                'user_id' => $this->integer()->notNull(),
                'railway_id' => $this->integer()->notNull(),
            ],
            $tableOptions
        );

        $this->createIndex('user_id_railway_id_index', '{{%user_railway}}', ['user_id', 'railway_id'], true);


        $this->addForeignKey(
            'fk_user_railway_user_id',
            '{{%user_railway}}',
            'user_id',
            '{{%user}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );


        $this->addForeignKey(
            'fk_user_railway_railway_id',
            '{{%user_railway}}',
            'railway_id',
            '{{%railway}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->addColumn(User::tableName(), 'railway_id', $this->integer()->null());


        $this->createIndex(
            'user_railway_id',
            User::tableName(),
            'railway_id'
        );


        $this->addForeignKey(
            'fk_user_railway_id',
            User::tableName(),
            'railway_id',
            'railway',
            'id',
            'RESTRICT',
            'CASCADE'
        );



        $this->dropForeignKey('fk_user_railway_user_id', '{{%user_railway}}');

        $this->dropForeignKey('fk_user_railway_railway_id', '{{%user_railway}}');

        $this->dropIndex('user_id_railway_id_index', '{{%user_railway}}');

        $this->dropTable('{{%user_railway}}');
    }

}
