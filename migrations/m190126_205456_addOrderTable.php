<?php

use app\models\db\Order;
use yii\db\Migration;

/**
 * Class m190126_205456_addOrderTable
 */
class m190126_205456_addOrderTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'meter_id' => $this->integer()->notNull(),
            'quantity' => $this->float()->notNull(),
            'date' => $this->date()->notNull(),
            'deleted' => $this->smallInteger()->defaultValue(Order::NOT_DELETED)
        ], $tableOptions);

        // creates index for column `author_id`
        $this->createIndex(
            'order_meter_id',
            'order',
            'meter_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk_order_meter_id',
            'order',
            'meter_id',
            'meter',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_order_meter_id', 'order');

        $this->dropIndex('order_meter_id', 'order');

        $this->dropTable('{{%order}}');
    }
}
