<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\Meter */
/* @var $form yii\widgets\ActiveForm */
/* @var $cityRepository \app\repositories\CityRepository */

?>

<div class="meter-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'provider_id')->dropDownList(\app\models\db\Provider::getList()) ?>

    <?= $form->field($model, 'zcode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_id')->dropDownList([null => 'Не задано'] + $cityRepository->listCitiesIdName()) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
