<?php

namespace app\models\db;

use app\dto\phone\PhonesDto;
use app\helpers\AuthHelper;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property int $id [int(11)]
 * @property string $xcode [varchar(255)]
 * @property string $auth_key [varchar(32)]
 * @property string $password_hash [varchar(255)]
 * @property string $password_reset_token [varchar(255)]
 * @property string $email [varchar(255)]
 * @property int $created_at [int(11)]
 * @property int $updated_at [int(11)]
 * @property string $password
 * @property string $authKey
 * @property int $deleted [smallint(6)]
 * @property Railway[] $railways
 * @property Phone[] $phones
 * @property string $egrpo
 * @property string $file_encode_public_key
 * @property string $title [varchar(255)]
 * @property string $position1_full_name [varchar(255)]
 * @property string $position2_full_name [varchar(255)]
 * @property int $signatory
 * @property string $contract_number [varchar(12)]
 * @property int $contract_conclusion_date [int(11)]
 * @property int $is_railway
 */
class User extends ActiveRecord implements IdentityInterface
{
    const NOT_DELETED = 0;
    const DELETED = 1;

    const ROLE_ADMINISTRATOR = 'administrator';
    const ROLE_USER = 'user';

    const RAILWAY = 1;
    const NOT_RAILWAY = 0;

    const SIGNATORY_DIRECTOR = 1;
    const SIGNATORY_ACCOUNTANT = 2;

    public function attributeLabels()
    {
        return [
            'title' => 'Название организации',
            'xcode' => 'X - код',
            'email' => 'Почта',
            'created_at' => 'Создан',
            'updated_at' => 'Отредактирован',
            'railway_id' => 'Ж/д',
            'egrpo' => 'ЕГРПО',
            'position1_full_name' => 'ФИО директора',
            'position2_full_name' => 'ФИО бухгалтера',
            'signatory' => 'Подписант',
            'contract_number' => 'Номер договора',
            'contract_conclusion_date' => 'Дата заключения договора',
            'is_railway' => 'Доступна ж/д',
        ];
    }

    public function getRoleName()
    {
        $roles = AuthHelper::listRoles();

        $userRoles = Yii::$app->authManager->getRolesByUser($this->id);

        foreach ($userRoles as $role) {
            if (isset($roles[$role->name])) {
                return $roles[$role->name];
            }
        }

        return '';
    }

    public function getRole()
    {
        $userRoles = Yii::$app->authManager->getRolesByUser($this->id);

        foreach ($userRoles as $role) {
            return $role->name;
        }

        return '';
    }

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [['is_railway', 'signatory'], 'integer'],
            [['egrpo',], 'string', 'length' => 8],
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'deleted' => self::NOT_DELETED]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'deleted' => self::NOT_DELETED,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getRailways()
    {
        return $this->hasMany(Railway::className(), ['id' => 'railway_id'])->viaTable('{{%user_railway}}', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhones()
    {
        return $this->hasMany(Phone::className(), ['user_id' => 'id']);
    }

    public static function getSignatoryList()
    {
        return [
            User::SIGNATORY_DIRECTOR => 'Директор',
            User::SIGNATORY_ACCOUNTANT => 'Бухгалтер',
        ];
    }

    public function signatoryName()
    {
        return self::getSignatoryList()[$this->signatory];
    }

    public function signatoryUserName()
    {
        return $this->signatory ? $this->position1_full_name : $this->position2_full_name;
    }
}
