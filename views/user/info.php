<?php

use app\models\db\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $user app\models\db\User */
/* @var $startDate string */
/* @var $meterList array */
/* @var $railwayList array */

$this->title = 'Пользователь: ' . $user->xcode;
$this->params['breadcrumbs'][] = 'Инфо';
?>
<div class="order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $user,
        'attributes' => array_merge([
            'xcode',
            'title',
            'email',
            'egrpo',
            'position1_full_name',
            'position2_full_name',
            [
                'attribute'  => 'signatory',
                'value'  => function ($model) {
                    /** @var User $model */

                    return User::getSignatoryList()[$model->signatory];
                },
            ],
            'created_at:datetime',
            'updated_at:datetime',
            'contract_number',
            'contract_conclusion_date:date',
            [
                'label' => 'Телефоны',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var User $model */

                    $phones = $model->phones;
                    $response = '';

                    foreach ($phones as $phone) {
                        $response .= $phone->comment . ' ' . $phone->number . '<br>';
                    }

                    return $response;
                }
            ],

        ], $user->is_railway ?
            [
                [
                    'label' => 'Ж/д',
                    'value' => function ($model) {
                        /** @var User $model */

                        $titles = ArrayHelper::getColumn($model->railways, 'title');
                        $response = implode(',', $titles);

                        return $response;
                    }
                ]
            ] : []),
    ]) ?>

    <?= Html::a('Редактировать', Url::to(['user/update']), ['class' => 'btn btn-success']) ?>

</div>
