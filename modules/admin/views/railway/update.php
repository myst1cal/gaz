<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Railway */

$this->title = 'Редактирование Ж/Д: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ж/Д', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="railway-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
