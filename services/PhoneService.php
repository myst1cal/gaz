<?php


namespace app\services;


use app\dto\phone\PhoneDto;
use app\dto\phone\PhonesDto;
use app\repositories\PhoneRepository;

class PhoneService
{

    public static function updateUserPhones(PhonesDto $phonesDto)
    {
        $userId = empty($phonesDto->phones[0]) ? null : $phonesDto->phones[0]->userId;

        if ($userId) {
            PhoneRepository::clearByUserId($userId);

            foreach ($phonesDto->phones as $phoneDto) {
                PhoneRepository::save($phoneDto);
            }
        }
    }

    public static function createDtoFromPhonesAndUserId(array $phones, int $userId)
    {
        $phonesDto = new PhonesDto();

        foreach ($phones as $phone) {
            if (!empty($phone['number'])) {
                $phoneDto = new PhoneDto();

                $phoneDto->userId = $userId;
                $phoneDto->number = $phone['number'];
                $phoneDto->comment = $phone['comment'];

                $phonesDto->phones[] = $phoneDto;
            }
        }

        return $phonesDto;
    }
}