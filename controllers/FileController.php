<?php

namespace app\controllers;

use app\models\db\User;
use app\models\forms\OrderFileUploadForm;
use app\models\searches\FileSearch;
use app\services\FileService;
use app\services\FileUploaderService;
use app\services\OrderService;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;


class FileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_USER],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        ini_set("memory_limit", "512M");
        set_time_limit(60);

        /** @var User $user */
        $user = Yii::$app->user->identity;

        $searchModel = new FileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $user->id);

        $fileForm = new OrderFileUploadForm();

        $encodeInstance = UploadedFile::getInstance($fileForm, 'orderFileEncode');

        if ($encodeInstance) {
            $fileForm->orderFileEncode = $encodeInstance;

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $orderFileDto = FileUploaderService::uploadOrderDocument($fileForm, $user);

                if ($orderFileDto) {
                    $file = FileService::createFromOrderFileUploadForm($orderFileDto, $user);
                    $userXlsFileData = FileService::parse($orderFileDto);
                    OrderService::confirm($userXlsFileData, $file);

                    Yii::$app->session->setFlash('success', 'Файл отправлен в обработку.');
                }

                $transaction->commit();
            } catch (\Throwable $throwable) {
                $transaction->rollBack();
                $encodeFileUrl = 'uploads/orders/' . $user->id . '/' . $fileForm->orderFileEncode->baseName . '.' . $fileForm->orderFileEncode->extension;

                if (file_exists($encodeFileUrl)) {
                    unlink($encodeFileUrl);
                }

                Yii::error($throwable->getMessage());
                Yii::$app->session->setFlash('danger', $throwable->getMessage());
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'fileForm' => $fileForm,
        ]);
    }
}
