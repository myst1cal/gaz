<?php

namespace app\modules\admin\models\forms;


use yii\base\Model;

class DayForm extends Model
{
    /** @var y-m-d $date */
    public $date;
    public $toXls;

    public function rules()
    {
        return [
            [['date'], 'string'],
            [['toXls'], 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'date' => 'Дата'
        ];
    }
}