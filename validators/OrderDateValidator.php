<?php


namespace app\validators;


use app\interfaces\IOrder;
use app\models\db\Meter;
use app\models\db\Order;
use app\services\OrderService;
use yii\base\Model;
use yii\validators\Validator;

class OrderDateValidator extends Validator
{
    /**
     * @param IOrder|Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!OrderService::canUpdate($model)) {
            $this->addError($model, $attribute, 'Нельзя сохранить с этой датой');
        }

        $order = Order::find()
            ->leftJoin(Meter::tableName(), 'meter.id=order.meter_id')
            ->where(['meter.zcode' => $model->getZcode()])
            ->andWhere(['order.date' => $model->getDate()])
            ->one();

        if (!empty($order)) {
            $this->addError($model, $attribute, 'Заказ на эту дату и z - код уже существует');
        }
    }
}