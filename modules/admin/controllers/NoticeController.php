<?php

namespace app\modules\admin\controllers;

use app\models\db\File;
use app\models\db\Notice;
use app\models\db\Order;
use app\models\db\User;
use app\models\searches\NoticeSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * NoticeController implements the CRUD actions for Notice model.
 */
class NoticeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_ADMINISTRATOR],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Notice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NoticeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewOrder($id)
    {
        return $this->render('view-order', [
            'model' => Order::findOne($id),
        ]);
    }

    public function actionViewFile($id)
    {
        return $this->render('view-file', [
            'model' => File::findOne($id),
        ]);
    }

    public function actionUpdateStatus()
    {
        $id = Yii::$app->request->get('id');
        $val = Yii::$app->request->get('val');

        $notice = Notice::findOne($id);
        $notice->status = $val ? Notice::SHOWED_NOTICE : Notice::NEW_NOTICE;
        $notice->save();
    }
}
