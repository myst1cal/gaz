<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\forms\DayForm;
use app\modules\admin\models\forms\MonthForm;
use app\modules\admin\services\ReportService;
use app\models\db\User;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class ReportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_ADMINISTRATOR],
                    ],
                ],
            ],
        ];
    }

    public function actionMonth()
    {
        $monthForm = new MonthForm();

        if ($monthForm->load(Yii::$app->request->post())) {
            $report = ReportService::getMonthReport($monthForm->date);

            if ($monthForm->toXls) {
                $sheet = ReportService::monthReportToXls($report);

                $filename = 'report-month-' . $report->date . '.xls';


                header("Cache-Control: private", false);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');

                $writer = new Xls($sheet);
                $writer->save('php://output');
            }

            return $this->render('month', ['model' => $monthForm, 'report' => $report]);
        }

        return $this->render('month', ['model' => $monthForm, 'report' => null]);
    }

    public function actionDay()
    {
        $dayForm = new DayForm();

        if ($dayForm->load(Yii::$app->request->post())) {
            $report = ReportService::getDayReport($dayForm->date);

            if ($dayForm->toXls) {
                $sheet = ReportService::dayReportToXls($report);

                $filename = 'report-day-' . $report->date . '.xls';
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');

                $writer = new Xls($sheet);
                $writer->save('php://output');
            }

            return $this->render('day', ['model' => $dayForm, 'report' => $report]);
        }

        return $this->render('day', ['model' => $dayForm, 'report' => []]);
    }
}
