<?php


namespace app\modules\admin\dto;


class ReportProviderDto
{
    /** @var string $title */
    public $title;

    /** @var ReportProviderItemDto[] $items */
    public $items;

    /** @var double $totalQuantity */
    public $totalQuantity;
}