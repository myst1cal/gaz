$(function () {
    $('.update-notice-status').click(function () {
        let id = $(this).attr('data-id');
        let val = $(this).prop('checked') ? 1 : 0;

        $.ajax({
            url: '/admin/notice/update-status',
            method: 'GET',
            data: {id, id, val: val}
        });
    });
});