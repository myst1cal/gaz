<?php

use app\helpers\AuthHelper;
use app\models\db\User;
use app\models\forms\UserForm;
use app\repositories\RailwayRepository;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model UserForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'xcode')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'role')->dropDownList(AuthHelper::listRoles()) ?>

    <?= $form->field($model, 'egrpo')->textInput() ?>

    <?= $form->field($model, 'contract_number')->textInput() ?>

    <?= $form->field($model, 'contract_conclusion_date')->widget(DatePicker::className(),
        [
            'language' => 'ru',
            'removeButton' => false,
            'options' => ['autocomplete' => 'off'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]
    ) ?>


    <?= $form->field($model, 'is_railway')->dropDownList($model->getIsRailwayList()) ?>

    <?= $form->field($model, 'railways')->checkboxList(RailwayRepository::getListIdTitle()) ?>

    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-pencil"></i> Ответственные лица</h4></div>
        <div class="panel-body">
            <div class="container-items">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <?= $form->field($model, 'position1_full_name')->textInput() ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <?= $form->field($model, 'position2_full_name')->textInput() ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <?= $form->field($model, 'signatory')->dropDownList(User::getSignatoryList()) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
