<?php

use yii\db\Migration;

/**
 * Class m190616_172416_addnNoticeTable
 */
class m190616_172416_addNoticeTable extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notice}}', [
            'id'          => $this->primaryKey(),
            'date'        => $this->dateTime(),
            'foreign_key' => $this->integer()->null(),
            'model'       => $this->string()->null(),
            'message'     => $this->string()->null(),
            'status'      => $this->tinyInteger(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%notice}}');
    }
}
