<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Файлы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($fileForm, 'orderFileEncode')->fileInput(['extensions' => 'p7s']) ?>
            </div>
            <div class="col-sm-2">
                <?= Html::submitButton('Загрузить', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end() ?>
        </div>

        <br>

        <?php $context = $this; ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'user_id',
                    'value' => function ($model) {
                        /** @var \app\models\db\File $model */

                        return $model->user->xcode ?: '';
                    }
                ],
                'upload_time',
                'file_name',
                [
                    'label' => '',
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var \app\models\db\File $model */

                        $html = Html::a('', '/' . $model->url . $model->file_name, ['title' => 'Файл', 'download' => true, 'class' => 'glyphicon glyphicon-download']);
                        $html .= Html::a('', '/' . $model->url . $model->file_name_encode, ['title' => 'Подписанный файл', 'download' => true, 'class' => 'glyphicon glyphicon-download']);

                        return $html;
                    }
                ],
                [
                    'label' => '',
                    'format' => 'raw',
                    'value' => function ($model) use ($context) {
                        /** @var \app\models\db\File $model */

                        $list = \app\models\db\OrderHistory::findAll(['file_id' => $model->id]);
                        $html = '';

                        foreach ($list as $item) {
                            $html .= $context->render('_order_history', ['orderHistory' => $item]);
                        }

                        return $html;
                    }
                ],
            ],
        ]); ?>
    </div>
