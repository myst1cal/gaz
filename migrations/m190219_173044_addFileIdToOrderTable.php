<?php

use yii\db\Migration;

/**
 * Class m190219_173044_addFileIdToOrderTable
 */
class m190219_173044_addFileIdToOrderTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'order',
            'file_id',
            $this->integer()->null()->after('railway_id')
        );

        $this->createIndex(
            'order_file_id',
            'order',
            'file_id'
        );

        $this->addForeignKey(
            'fk_order_file_id',
            'order',
            'file_id',
            'file',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_order_file_id', 'order');

        $this->dropIndex('order_file_id', 'order');

        $this->dropColumn('order', 'file_id');
    }
}
