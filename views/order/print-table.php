<?php

use app\models\db\Meter;
use app\models\db\User;

/** @var Meter $meter */

?>
<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <style>
        <!--
        table {
            mso-displayed-decimal-separator: "\,";
            mso-displayed-thousand-separator: " ";
        }

        @page {
            margin: .98in .16in .98in .75in;
            mso-header-margin: .51in;
            mso-footer-margin: .51in;
            mso-page-orientation: landscape;
        }

        -->
        tr {
            mso-height-source: auto;
        }

        col {
            mso-width-source: auto;
        }

        br {
            mso-data-placement: same-cell;
        }

        .style0 {
            mso-number-format: General;
            text-align: left;
            vertical-align: bottom;
            white-space: nowrap;
            mso-rotate: 0;
            mso-background-source: auto;
            mso-pattern: auto;
            color: windowtext;
            font-size: 8.0pt;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            font-family: Arial, sans-serif;
            mso-font-charset: 204;
            border: none;
            mso-protection: locked visible;
            mso-style-name: ќбычный;
            mso-style-id: 0;
        }

        .font0 {
            color: windowtext;
            font-size: 8.0pt;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            font-family: Arial, sans-serif;
            mso-font-charset: 204;
        }

        .font13 {
            font-size: 8.0pt;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            font-family: Arial, sans-serif;
            mso-font-charset: 204;
        }

        td {
            mso-style-parent: style0;
            padding-top: 1px;
            padding-right: 1px;
            padding-left: 1px;
            mso-ignore: padding;
            color: windowtext;
            font-size: 8.0pt;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            font-family: Arial, sans-serif;
            mso-font-charset: 204;
            mso-number-format: General;
            text-align: general;
            border: none;
            mso-background-source: auto;
            mso-pattern: auto;
            mso-protection: locked visible;
            white-space: nowrap;
            mso-rotate: 0;
        }

        .xl65 {
            mso-style-parent: style0;
            font-size: 12.0pt;
            font-style: italic;
        }

        .xl66 {
            mso-style-parent: style0;
            font-size: 10.0pt;
        }

        .xl67 {
            mso-style-parent: style0;
            font-size: 10.0pt;
            font-weight: 700;
            border: .5pt solid windowtext;
            white-space: normal;
        }

        .xl68 {
            mso-style-parent: style0;
            font-size: 10.0pt;
            font-weight: 700;
            border: .5pt solid windowtext;
        }

        .xl69 {
            mso-style-parent: style0;
            font-size: 12.0pt;
        }

        .xl70 {
            mso-style-parent: style0;
            font-size: 12.0pt;
            font-weight: 700;
        }

        .xl71 {
            mso-style-parent: style0;
            font-size: 10.0pt;
            font-weight: 700;
            text-align: center;
            border: .5pt solid windowtext;
            white-space: normal;
        }

        .xl72 {
            mso-style-parent: style0;
            font-size: 12.0pt;
            font-weight: 700;
            text-decoration: underline;
            text-underline-style: single;
            text-align: center;
            vertical-align: middle;
        }

        .xl73 {
            mso-style-parent: style0;
            font-size: 12.0pt;
            font-weight: 700;
            text-decoration: underline;
            text-underline-style: single;
            text-align: center-across;
            vertical-align: middle;
        }

        .xl74 {
            mso-style-parent: style0;
            font-size: 10.0pt;
            text-align: center-across;
            vertical-align: middle;
        }

        .xl75 {
            mso-style-parent: style0;
            font-family: "Times New Roman", serif;
            mso-font-charset: 204;
        }

        .xl76 {
            mso-style-parent: style0;
            font-family: "Times New Roman", serif;
            mso-font-charset: 204;
            border-top: none;
            border-right: none;
            border-bottom: .5pt solid windowtext;
            border-left: none;
        }

        .xl77 {
            mso-style-parent: style0;
            font-size: 10.0pt;
            font-weight: 700;
            mso-number-format: "Short Date";
            text-align: center-across;
            border-top: none;
            border-right: none;
            border-bottom: .5pt solid windowtext;
            border-left: none;
        }

        .xl78 {
            mso-style-parent: style0;
            text-align: center-across;
            border-top: none;
            border-right: none;
            border-bottom: .5pt solid windowtext;
            border-left: none;
        }

        .xl79 {
            mso-style-parent: style0;
            font-size: 12.0pt;
            font-weight: 700;
            font-family: "Times New Roman", serif;
            mso-font-charset: 204;
        }

        .xl80 {
            mso-style-parent: style0;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
            mso-font-charset: 204;
        }

        .xl81 {
            mso-style-parent: style0;
            font-size: 10.0pt;
            font-weight: 700;
            mso-number-format: "\#\,\#\#0\.000";
            border: .5pt solid windowtext;
        }

        .xl82 {
            mso-style-parent: style0;
            /*font-size: 10.0pt;*/
            mso-number-format: "\#\,\#\#0\.000";
            border: .5pt solid windowtext;
            mso-rotate: 90;
        }

        .xl83 {
            mso-style-parent: style0;
            font-size: 10.0pt;
            border: .5pt solid windowtext;
            white-space: normal;
        }

        .xl84 {
            mso-style-parent: style0;
            font-size: 10.0pt;
        }

        .xl85 {
            mso-style-parent: style0;
            font-size: 10.0pt;
            font-weight: 700;
            text-align: center;
            vertical-align: middle;
            border-top: .5pt solid windowtext;
            border-right: none;
            border-bottom: .5pt solid windowtext;
            border-left: .5pt solid windowtext;
        }

        .xl86 {
            mso-style-parent: style0;
            text-align: center;
            vertical-align: middle;
            border-top: .5pt solid windowtext;
            border-right: none;
            border-bottom: .5pt solid windowtext;
            border-left: none;
        }

        .xl87 {
            mso-style-parent: style0;
            text-align: center;
            vertical-align: middle;
            border-top: .5pt solid windowtext;
            border-right: .5pt solid windowtext;
            border-bottom: .5pt solid windowtext;
            border-left: none;
        }

        .xl88 {
            mso-style-parent: style0;
            font-size: 12.0pt;
            font-weight: 700;
            border-top: none;
            border-right: none;
            border-bottom: .5pt solid windowtext;
            border-left: none;
            white-space: normal;
        }

        .xl89 {
            mso-style-parent: style0;
            border-top: none;
            border-right: none;
            border-bottom: .5pt solid windowtext;
            border-left: none;
            white-space: normal;
        }

        .xl90 {
            mso-style-parent: style0;
            font-size: 12.0pt;
            font-weight: 700;
            border-top: none;
            border-right: none;
            border-bottom: .5pt solid windowtext;
            border-left: none;
            white-space: normal;
        }

        .xl91 {
            mso-style-parent: style0;
            border-top: none;
            border-right: none;
            border-bottom: .5pt solid windowtext;
            border-left: none;
            white-space: normal;
        }

    </style>
</head>

<body link=blue vlink=purple>

<table border=0 cellpadding=0 cellspacing=0 width=1189 style='border-collapse:
 collapse;table-layout:fixed;width:899pt'>
    <col width=68 style='mso-width-source:userset;mso-width-alt:3108;width:51pt'>
    <col width=98 style='mso-width-source:userset;mso-width-alt:4461;width:73pt'>
    <col width=33 span=31 style='mso-width-source:userset;mso-width-alt:1499;width:25pt'>
    <tr height=18 style='height:13.2pt'>
        <td height=18 class=xl66 width=68 style='height:13.2pt;width:51pt'></td>
        <td class=xl66 width=98 style='width:73pt'></td>
        <td colspan=3 ></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td colspan=5 width=165 style='mso-ignore:colspan;width:125pt'>Доповнення до Додатку<span style='mso-spacerun:yes'>  </span>№ 2
        </td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
    </tr>
    <tr height=18 style='height:13.2pt'>
        <td height=18 class=xl66 style='height:13.2pt'>Вих</td>
        <td class=xl66>
            <span style='mso-spacerun:yes'>                  </span>від<span style='mso-spacerun:yes'> </span></span>
        </td>
        <td colspan=16 style='mso-ignore:colspan;width:75pt'>___.___.<?= date('Y') ?></td>
        <td colspan=11 style='mso-ignore:colspan'>
            до договору №
            <font class="font13">
                <?= empty($meter->user->contract_number) ? "НЕ ЗАПОЛНЕН!!!" : $meter->user->contract_number ?> від <?= empty($meter->user->contract_conclusion_date) ? "ДАТА НЕ ЗАПОЛНЕНА!!!" : date('d.m.Y', strtotime($meter->user->contract_conclusion_date)) ?> р.
            </font>
            <font class="font0"> на постачання природного газу.</font>
        </td>
        <td></td>
    </tr>
    <tr height=14 style='height:10.2pt'>
        <td height=14 colspan=21 style='height:10.2pt;mso-ignore:colspan'></td>
        <td colspan=10 style='mso-ignore:colspan'>(відповідно Кодексу ГТС та Правил
            постачання природного газу)<span style='mso-spacerun:yes'>  </span></td>
        <td colspan=2 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=14 style='height:10.2pt'>
        <td height=14 colspan=21 style='height:10.2pt;mso-ignore:colspan'></td>
        <td></td>
        <td colspan=11 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=21 style='height:15.6pt'>
        <td height=21 colspan=32 class=xl73 align=center style='height:15.6pt;
  mso-ignore:colspan'>ЗАЯВКА НА ОЧІКУВАНИЙ (ПЛАНОВИЙ) ОБСЯГ СПОЖИВАННЯ<span
                    style='mso-spacerun:yes'>  </span>ПРИРОДНОГО ГАЗУ
        </td>
        <td class=xl72></td>
    </tr>
    <tr height=14 style='height:10.2pt'>
        <td height=14 colspan=33 style='height:10.2pt;mso-ignore:colspan'></td>
    </tr>
    <tr height=21 style='mso-height-source:userset;height:15.75pt'>
        <td height=21 class=xl65 colspan=5 style='height:15.75pt;mso-ignore:colspan'>Найменування
            Постачальника:
        </td>
        <td colspan=2 style='mso-ignore:colspan'></td>
        <td colspan=26 class=xl88 width=858 style='width:650pt'>ТОВ<span
                    style='mso-spacerun:yes'>  </span>&quot;ЮГ-ГАЗ&quot;
        </td>
    </tr>
    <tr height=26 style='mso-height-source:userset;height:19.5pt'>
        <td height=26 class=xl65 colspan=4 style='height:19.5pt;mso-ignore:colspan'>Наименування
            Споживача:
        </td>
        <td colspan=3 style='mso-ignore:colspan'></td>
        <td colspan=26 class=xl90 width=858 style='width:650pt'><?= $meter->user->title ?></td>
    </tr>
    <tr height=21 style='mso-height-source:userset;height:15.75pt'>
        <td height=21 class=xl65 colspan=7 style='height:15.75pt;mso-ignore:colspan'>
            Адреса постачання<span style='mso-spacerun:yes'>  </span>(адреса вузла обліку):
        </td>
        <td colspan=26 class=xl90 width=858 style='width:650pt'><?= $meter->address ?></td>
    </tr>
    <tr height=21 style='mso-height-source:userset;height:15.75pt'>
        <td height=21 class=xl65 colspan=2 style='height:15.75pt;mso-ignore:colspan'>
            ЕІС-код Споживача
        </td>
        <td colspan=5 style='mso-ignore:colspan'></td>
        <td colspan=26 class=xl90 width=858 style='width:650pt'><?= $meter->user->xcode ?></td>
    </tr>
    <tr height=21 style='mso-height-source:userset;height:15.75pt'>
        <td height=21 class=xl65 colspan=6 style='height:15.75pt;mso-ignore:colspan'>
            ЕІС-код точки комерційного обліку
        </td>
        <td></td>
        <td colspan=26 class=xl90 width=858 style='width:650pt'><?= $meter->zcode ?: '' ?></td>
    </tr>
    <tr height=14 style='height:10.2pt'>
        <td height=14 colspan=33 style='height:10.2pt;mso-ignore:colspan'></td>
    </tr>
    <tr height=18 style='height:13.2pt'>
        <td height=18 class=xl66 colspan=3 style='height:13.2pt;mso-ignore:colspan'>
            Дата початку<span style='mso-spacerun:yes'>  </span>постачання:
        </td>
        <td colspan=5 style='mso-ignore:colspan'></td>
        <td colspan=3 class=xl77 align=center style='mso-ignore:colspan'><?= $startDate ?></td>
        <td colspan=22 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=18 style='height:13.2pt'>
        <td height=18 class=xl66 colspan=3 style='height:13.2pt;mso-ignore:colspan'>
            Дата закінчення постачання:
        </td>
        <td colspan=5 style='mso-ignore:colspan'></td>
        <td colspan=3 class=xl77 align=center style='mso-ignore:colspan'><?= $endDate ?></td>
        <td colspan=<?= count($days) - 9 ?> style='mso-ignore:colspan'></td>
    </tr>
    <tr height=14 style='height:10.2pt'>
        <td height=14 colspan="31" style='height:10.2pt;mso-ignore:colspan'></td>
    </tr>
    <tr height=35 style='height:26.4pt'>
        <td height=35 class=xl67 width=68 style='height:26.4pt;width:51pt'>Місяць</td>
        <td class=xl71 width=98 style='border-left:none;width:73pt'>
            Обсяг всього
            <br>
            (тис.м.куб)
        </td>
        <td colspan="31" class=xl85 style='border-right:.5pt solid black;border-left:none'>
            Обсяг добової норми<span style='mso-spacerun:yes'>  </span>(в тис.м.куб)
        </td>
    </tr>
    <tr height=20 style='mso-height-source:userset;height:15.0pt'>
        <td height=20 class=xl67 width=68 style='height:15.0pt;border-top:none;width:51pt'>&nbsp;
        </td>
        <td class=xl67 width=98 style='border-top:none;border-left:none;width:73pt'>&nbsp;</td>
        <?php foreach (range(1, 31) as $day): ?>
            <td class=xl68 style='border-top:none;border-left:none'><?= $day ?></td>
        <?php endforeach; ?>
    </tr>
    <?php foreach ($dataByMonths as $dataByMonth): ?>
        <tr height=58 style='mso-height-source:userset;height:44.25pt'>
            <td height=58 class=xl83 width=68 style='height:44.25pt;border-top:none;width:51pt'><?= $dataByMonth['month'] . '/' . $dataByMonth['year'] ?></td>
            <td class=xl81 align=right style='border-top:none;border-left:none'><?= number_format($dataByMonth['sum'],
                    3) ?></td>
            <?php foreach (range(1,31) as $day): ?>
                <td class=xl82 style='border-top:none;border-left:none'><?= $dataByMonth['orderByDay'][$day] ?? 0 ?></td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    <tr height=20 style='height:15.0pt'>
        <td height=20 style='height:15.0pt'></td>
        <td class=xl69 colspan=21 style='mso-ignore:colspan'>
            *Дана<span style='mso-spacerun:yes'>  </span>заявка заповнюється Споживачем відносно кожної точки
            комерційного обліку (за необхідності)
        </td>
        <td colspan=11 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=14 style='height:10.2pt'>
        <td height=14 colspan=33 style='height:10.2pt;mso-ignore:colspan'></td>
    </tr>
    <tr height=21 style='height:15.6pt'>
        <td height=21 style='height:15.6pt'></td>
        <td class=xl70>Споживач:</td>
        <td colspan=5 style='mso-ignore:colspan'></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan=5 style='mso-ignore:colspan'></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan=12 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=30 style='mso-height-source:userset;height:22.5pt'>
        <td height=30 style='height:22.5pt'></td>
        <td class=xl79 colspan=3 style='mso-ignore:colspan'><?= $meter->user->title ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan=6 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=14 style='height:10.2pt'>
        <td height=14 style='height:10.2pt'></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan=6 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=21 style='height:15.6pt'>
        <td height=21 style='height:15.6pt'></td>
        <td class=xl80><?= $meter->user->signatoryName() . ' ' . $meter->user->signatoryUserName() ?></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan=6 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=14 style='height:10.2pt'>
        <td height=14 style='height:10.2pt'></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan=6 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=14 style='height:10.2pt'>
        <td height=14 style='height:10.2pt'></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan=6 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=21 style='mso-height-source:userset;height:15.75pt'>
        <td height=21 style='height:15.75pt'></td>
        <td class=xl76>&nbsp;</td>
        <td class=xl76>&nbsp;</td>
        <td class=xl76>&nbsp;</td>
        <td class=xl76>&nbsp;</td>
        <td class=xl76>&nbsp;</td>
        <td class=xl80>ПІБ</td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl75></td>
        <td class=xl70></td>
        <td colspan=22 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=18 style='height:13.2pt'>
        <td height=18 style='height:13.2pt'></td>
        <td colspan=5 class=xl74 align=center style='mso-ignore:colspan'>підпис печатка
        </td>
        <td></td>
        <td class=xl74></td>
        <td class=xl74></td>
        <td class=xl74></td>
        <td></td>
        <td colspan=22 style='mso-ignore:colspan'></td>
    </tr>

    <tr height=14 style='height:10.2pt'>
        <td height=14 style='height:10.2pt'></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan=22 style='mso-ignore:colspan'></td>
    </tr>
    <tr height=14 style='height:10.2pt'>
        <td height=14 style='height:10.2pt'></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan=23 style='mso-ignore:colspan'></td>
    </tr>
    <![if supportMisalignedColumns]>
    <tr height=0 style='display:none'>
        <td width=68 style='width:51pt'></td>
        <td width=98 style='width:73pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
        <td width=33 style='width:25pt'></td>
    </tr>
    <![endif]>
</table>

</body>

</html>
