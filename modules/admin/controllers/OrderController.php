<?php

namespace app\modules\admin\controllers;

use app\dto\date\DateDto;
use app\models\db\Meter;
use app\models\db\Order;
use app\models\db\Provider;
use app\models\db\Railway;
use app\models\db\User;
use app\modules\admin\models\searches\OrderSearch;
use app\services\OrderService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_ADMINISTRATOR],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionListOrdersByProvider($providerId)
    {
        $query = Order::find()
            ->leftJoin(Meter::tableName(), 'meter_id=meter.id')
            ->where(['meter.provider_id' => $providerId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $provider = Provider::findOne($providerId);

        return $this->render('list_orders_by_provider', ['dataProvider' => $dataProvider, 'provider' => $provider]);
    }

    public function actionListOrdersByRailway($railwayId)
    {
        $query = Order::find()->where(['railway_id' => $railwayId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $railway = Railway::findOne($railwayId);

        return $this->render('list_orders_by_railway', ['dataProvider' => $dataProvider, 'railway' => $railway]);
    }

    public function actionPrint()
    {
        $dateFrom = Yii::$app->request->post('date_from');
        $dateTo = Yii::$app->request->post('date_to');
        $meterId = Yii::$app->request->post('meter_id');

        if (strlen($dateFrom) == 7) {
            $dateFrom = $dateFrom . '-01';
            $daysInMonth = date('t', strtotime($dateTo . '-01'));
            $dateTo = $dateTo . '-' . $daysInMonth;
        }

        $data = OrderService::getUserXlsData($dateFrom, $dateTo, $meterId);

        $spreadsheet = new Spreadsheet();
        try {
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValue('A' . 1, 'Дата');
            $sheet->setCellValue('B' . 1, 'Номинация, тыс. м3');
            $sheet->setCellValue('C' . 1, 'Реноминация, тыс. м3');
            $sheet->setCellValue('D' . 1, 'z - код');

            $rowNumber = 2;
            if ($data->rows) {
                foreach ($data->rows as $rowDto) {
                    $sheet->setCellValue('A' . $rowNumber, $rowDto->date);
                    $sheet->setCellValue('B' . $rowNumber, $rowDto->nomination);
                    $sheet->setCellValue('C' . $rowNumber, $rowDto->renomination);
                    $sheet->setCellValue('D' . $rowNumber, $rowDto->zcode);

                    $rowNumber++;
                }
            }

            $filename = $dateFrom . '-' . $dateTo . '.xls';
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');

            $writer = new Xls($spreadsheet);
            $writer->save('php://output');
        } catch (Exception $e) {
            Yii::error($e->getMessage());
        }
    }

    public function actionCurrentMonth()
    {
        $meterId = Yii::$app->request->get('meter_id');

        $this->layout = 'withoutBorder';

        $orders = Order::find()
            ->where(new Expression('MONTH(date) = MONTH(CURRENT_DATE()) AND YEAR(date) = YEAR(CURRENT_DATE())'))
            ->andWhere(['meter_id' => $meterId])
            ->orderBy('date')
            ->all();

        $nominationSum = 0;
        $renominationSum = 0;
        $ordersByDay = [];

        $days = date('t');
        foreach (range(1, $days) as $day) {
            $ordersByDay[$day] = date('Y-m-') . $day;
        }

        foreach ($orders as $order) {
            $nominationSum += $order->nomination;
            $renominationSum += (double)$order->renomination ?: $order->nomination;
            $ordersByDay[$order->getDay()] = $order;
        }

        $meter = Meter::findOne($meterId);
        if (Yii::$app->request->post('print')) {
            $sum = Yii::$app->request->post('nomination') ? $nominationSum : $renominationSum;
            foreach (range(1, date('t')) as $day) {
                $ordersByDay[$day] = 0;
            }
            foreach ($orders as $order) {
                $ordersByDay[$order->getDay()] = Yii::$app->request->post('nomination') ? $order->nomination ?: 0 : ((double)$order->renomination ?: $order->nomination ?: 0);
            }

            $startDate = "1." . date('m') . "." . date('Y');
            $endDate = $days . "." . date('m') . "." . date('Y');

            return $this->renderPartial('print-table',
                [
                    'dataByMonths' => [
                        'days'       => range(1, $days),
                        'meter'      => $meter,
                        'sum'        => $sum,
                        'orderByDay' => $ordersByDay,
                        'endDay'     => $days,
                        'month'      => date('m'),
                        'year'       => date('Y'),
                    ],
                    'endDate' => $endDate,
                    'startDate' => $startDate,
                    'days' => $days,
                    'meter' => $meter,
                ]
            );
        }

        return $this->render('table', [
            'meter'           => $meter,
            'date'            => date('m.Y'),
            'days'            => range(1, date('t')),
            'nominationSum'   => $nominationSum,
            'renominationSum' => $renominationSum,
            'orders'          => $ordersByDay,
        ]);
    }

    public function actionNextMonth()
    {
        $this->layout = 'withoutBorder';

        $meterId = Yii::$app->request->get('meter_id');
        /** @var User $user */

        $year = date('Y');
        $month = (string)date('m');
        if ($month == 12) {
            $month = '01';
            $year += 1;
        } else {
            $month += 1;
        }

        /** @var Order[] $orders */
        $orders = Order::find()
            ->where(new Expression('CAST(MONTH(date) AS UNSIGNED) = ' . $month . ' AND YEAR(date) = ' . $year))
            ->andWhere(['meter_id' => $meterId])
            ->orderBy('date')
            ->all();

        $nominationSum = 0;
        $renominationSum = 0;
        $ordersByDay = [];

        $days = date('t', strtotime("$year-$month-01"));
        foreach (range(1, $days) as $day) {
            $ordersByDay[$day] = "$year-$month-" . $day;
        }

        foreach ($orders as $order) {
            $nominationSum += $order->nomination;
            $renominationSum += (double)$order->renomination ?: $order->nomination;
            $ordersByDay[$order->getDay()] = $order;
        }

        $meter = Meter::findOne($meterId);
        if (Yii::$app->request->post('print')) {
            $sum = Yii::$app->request->post('nomination') ? $nominationSum : $renominationSum;
            foreach (range(1, $days) as $day) {
                $ordersByDay[$day] = 0;
            }
            foreach ($orders as $order) {
                $ordersByDay[$order->getDay()] = Yii::$app->request->post('nomination') ? $order->nomination ?: 0 : ((double)$order->renomination ?: $order->nomination ?: 0);
            }

            $startDate = "1." . $month . "." . $year;
            $endDate = $days . "." . $month . "." . $year;

            return $this->renderPartial('print-table',
                [
                    'dataByMonths' => [
                        'days' => range(1, $days),
                        'meter' => $meter,
                        'sum' => $sum,
                        'orderByDay' => $ordersByDay,
                        'endDay' => $days,
                        'month' => $month,
                        'year' => $year,
                    ],
                    'endDate' => $endDate,
                    'meter' => $meter,
                    'days' => $days,
                    'startDate' => $startDate,

                ]
            );
        }

        return $this->render('table', [
            'meter'           => $meter,
            'date'            => $month . '.' . $year,
            'days'            => range(1, $days),
            'nominationSum'   => $nominationSum,
            'renominationSum' => $renominationSum,
            'orders'          => $ordersByDay,
        ]);
    }

    public function actionPrintTable()
    {
        $meterId = Yii::$app->request->post('meter_id');
        $from = Yii::$app->request->post('date_from');
        $to = Yii::$app->request->post('date_to');


        $dates = [];
        list($yearFrom, $monthFrom) = explode('-', $from);
        list($yearTo, $monthTo) = explode('-', $to);

        for ($i = 0, $break = true, $currentYear = $yearFrom; $break; $currentYear++, $i++) {
            for ($currentMonth = $i ? 1 : $monthFrom; $currentMonth < 13 && $break; $currentMonth++) {
                if ($currentMonth == $monthTo && $currentYear == $yearTo) {
                    $break = false;
                }

                $date = new DateDto();
                $date->year = $currentYear;
                $date->month = $currentMonth;

                $dates[] = $date;
            }
        }

        $dataByMonths = [];
        $meter = Meter::findOne($meterId);

        $endDay = 1;
        foreach ($dates as $date) {

            $data = OrderService::getOrdersByDayAndSums($date, $meterId);
            $sum = $data['sums']['renomination'];

            $endDay = $data['daysInMonth'];

            $dataByMonths[$date->year . '-' . $date->month] = [
                'days' => range(1, $data['daysInMonth']),
                'meter' => $meter,
                'sum' => $sum,
                'orderByDay' => $data['print']['days'],
                'endDay' => $data['daysInMonth'],
                'month' => $date->month,
                'year' => $date->year,
            ];
        }

        $startDate = "1.$monthFrom.$yearFrom";
        $endDate = "$endDay.$monthTo.$yearTo";

        return $this->renderPartial('print-table',
            [
                'dataByMonths' => $dataByMonths,
                'meter' => $meter,
                'days' => range(1, 31),
                'endDate' => $endDate,
                'startDate' => $startDate,
            ]
        );
    }

    public function actionTable()
    {
        $this->layout = 'withoutBorder';

        $meterId = Yii::$app->request->get('meter_id');
        $print = Yii::$app->request->post('print');
        $printDate = Yii::$app->request->post('print_date');
        $from = Yii::$app->request->get('date_from');
        $to = Yii::$app->request->get('date_to');

        $dates = [];
        list($yearFrom, $monthFrom) = explode('-', $from);
        list($yearTo, $monthTo) = explode('-', $to);

        for ($i = 0, $break = true, $currentYear = $yearFrom; $break; $currentYear++, $i++) {
            for ($currentMonth = $i ? 1 : $monthFrom; $currentMonth < 13 && $break; $currentMonth++) {
                if ($currentMonth == $monthTo && $currentYear == $yearTo) {
                    $break = false;
                }

                $date = new DateDto();
                $date->year = $currentYear;
                $date->month = $currentMonth;

                $dates[] = $date;
            }
        }

        if (Yii::$app->request->isPost && !$print) {
            $nominations = Yii::$app->request->post('nomination', []);
            $renominations = Yii::$app->request->post('renomination', []);

            foreach ($nominations as $yearMonth => $nominationMonthData) {
                $date = new DateDto();
                list($date->year, $date->month) = explode('-', $yearMonth);

                OrderService::saveNominations($nominationMonthData, $meterId, $date);
            }
            foreach ($renominations as $yearMonth => $renominationMonthData) {
                $date = new DateDto();
                list($date->year, $date->month) = explode('-', $yearMonth);

                OrderService::saveRenominations($renominationMonthData, $meterId, $date);
            }

            Yii::$app->session->setFlash('success', 'Изменения сохранены.');
        }

        foreach ($dates as $date) {

            $data = OrderService::getOrdersByDayAndSums($date, $meterId);
            if ($print && $printDate == $date->year . '-' . $date->month) {
                $meter = Meter::findOne($meterId);
                $sum = Yii::$app->request->post('nomination') ? $data['sums']['nomination'] : $data['sums']['renomination'];

                $startDate = "1.{$date->month}.{$date->year}";
                $endDate = "{$data['daysInMonth']}.{$date->month}.{$date->year}";

                return $this->renderPartial('print-table',
                    [
                        'dataByMonths' => [[
                            'days' => range(1, $data['daysInMonth']),
                            'meter' => $meter,
                            'sum' => $sum,
                            'orderByDay' => $data['print']['days'],
                            'month' => $date->month,
                            'year' => $date->year,
                        ]],
                        'endDate' => $endDate,
                        'startDate' => $startDate,
                        'days' => range(1, $data['daysInMonth']),
                        'meter' => $meter,
                    ]
                );
            }

            $dataByMonth[$date->year . '-' . $date->month] = [
                'from' => $from,
                'to' => $to,
                'meterId' => $meterId,
                'date' => $date->month . '.' . $date->year,
                'days' => range(1, $data['daysInMonth']),
                'nominationSum' => $data['sums']['nomination'],
                'renominationSum' => $data['sums']['renomination'],
                'orders' => $data['days'],];
        }
        return $this->render('tables', ['meterId' => $meterId, 'from' => $from, 'to' => $to, 'dataByMonth' => $dataByMonth]);
    }
}
