<?php

use yii\db\Migration;

/**
 * Class m200820_132805_order_history
 */
class m200820_132805_order_history extends Migration
{
    public function safeUp()
    {
        $this->createTable('order_history', [
            'id' => $this->primaryKey(),
            'file_id' => $this->integer()->null(),
            'meter_id' => $this->integer()->null(),
            'date_time' => $this->integer()->comment('Время сохранения'),
            'year' => $this->string(4),
            'month' => $this->string(2),
            'day_1' => $this->decimal(10, 3)->defaultValue(0),
            'day_2' => $this->decimal(10, 3)->defaultValue(0),
            'day_3' => $this->decimal(10, 3)->defaultValue(0),
            'day_4' => $this->decimal(10, 3)->defaultValue(0),
            'day_5' => $this->decimal(10, 3)->defaultValue(0),
            'day_6' => $this->decimal(10, 3)->defaultValue(0),
            'day_7' => $this->decimal(10, 3)->defaultValue(0),
            'day_8' => $this->decimal(10, 3)->defaultValue(0),
            'day_9' => $this->decimal(10, 3)->defaultValue(0),
            'day_10' => $this->decimal(10, 3)->defaultValue(0),
            'day_11' => $this->decimal(10, 3)->defaultValue(0),
            'day_12' => $this->decimal(10, 3)->defaultValue(0),
            'day_13' => $this->decimal(10, 3)->defaultValue(0),
            'day_14' => $this->decimal(10, 3)->defaultValue(0),
            'day_15' => $this->decimal(10, 3)->defaultValue(0),
            'day_16' => $this->decimal(10, 3)->defaultValue(0),
            'day_17' => $this->decimal(10, 3)->defaultValue(0),
            'day_18' => $this->decimal(10, 3)->defaultValue(0),
            'day_19' => $this->decimal(10, 3)->defaultValue(0),
            'day_20' => $this->decimal(10, 3)->defaultValue(0),
            'day_21' => $this->decimal(10, 3)->defaultValue(0),
            'day_22' => $this->decimal(10, 3)->defaultValue(0),
            'day_23' => $this->decimal(10, 3)->defaultValue(0),
            'day_24' => $this->decimal(10, 3)->defaultValue(0),
            'day_25' => $this->decimal(10, 3)->defaultValue(0),
            'day_26' => $this->decimal(10, 3)->defaultValue(0),
            'day_27' => $this->decimal(10, 3)->defaultValue(0),
            'day_28' => $this->decimal(10, 3)->defaultValue(0),
            'day_29' => $this->decimal(10, 3)->defaultValue(0),
            'day_30' => $this->decimal(10, 3)->defaultValue(0),
            'day_31' => $this->decimal(10, 3)->defaultValue(0),
        ]);

        $this->createIndex('meter_id_date_time_i', 'order_history', ['meter_id', 'date_time']);
    }

    public function safeDown()
    {
        $this->dropIndex('meter_id_date_time_i', 'order_history');

        $this->dropTable('order_history');
    }
}
