<?php

use app\models\db\User;
use yii\db\Migration;

/**
 * Class m190203_131928_addRailwayColumnToUserTable
 */
class m190203_131928_addRailwayColumnToUserTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn(User::tableName(), 'railway_id', $this->integer()->null());


        $this->createIndex(
            'user_railway_id',
            User::tableName(),
            'railway_id'
        );


        $this->addForeignKey(
            'fk_user_railway_id',
            User::tableName(),
            'railway_id',
            'railway',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_railway_id', User::tableName());

        $this->dropIndex('user_railway_id', User::tableName());

        $this->dropColumn(User::tableName(), 'railway_id');
    }
}
