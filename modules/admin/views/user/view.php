<?php

use app\models\db\User;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\db\User */

$this->title = $model->xcode;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('К списку', ['index', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'xcode',
            'title',
            'email',
            'egrpo',
            'created_at:datetime',
            'updated_at:datetime',
            'position1_full_name',
            'position2_full_name',
            [
                'attribute'  => 'signatory',
                'value'  => function ($model) {
                    /** @var User $model */

                    return User::getSignatoryList()[$model->signatory];
                },
            ],
            'contract_number',
            'contract_conclusion_date:date',
            [
                'label'  => 'Телефоны',
                'format' => 'raw',
                'value'  => function ($model) {
                    /** @var User $model */

                    $phones = $model->phones;
                    $response = '';

                    foreach ($phones as $phone) {
                        $response .= $phone->comment . ' ' . $phone->number;
                    }

                    return $response;
                },
            ],
            [
                'label' => 'Ж/д',
                'value' => function ($model) {
                    /** @var User $model */

                    $titles = ArrayHelper::getColumn($model->railways, 'title');
                    $response = implode(',', $titles);

                    return $response;
                },
            ],
        ],
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'style'=>'overflow: auto; word-wrap: break-word;'
        ],
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'zcode',
            [
                'attribute' => 'title',
                'format' => 'ntext',
            ],
            'address:ntext',

            [
                'class'    => 'yii\grid\ActionColumn',
                'contentOptions'=>['style'=>'width: 130px;'],
                'template' => '{current} {next} {table}',
                'buttons'  => [
                    'current' => function ($url, $model) {
                        return Html::a('Таблица(текущий месяц)', Url::to(['order/current-month', 'meter_id' => $model->id]), ['title' => 'Таблица(текущий месяц)', 'class' => 'btn btn-success']);
                    },
                    'next' => function ($url, $model) {
                        return Html::a('Таблица(следующий месяц)', Url::to(['order/next-month', 'meter_id' => $model->id]), ['title' => 'Таблица(следующий месяц)', 'class' => 'btn btn-info']);
                    },
                    'table' => function ($url, $model) {
                        return Html::a('Таблица(интервал)', Url::to(['order/table', 'meter_id' => $model->id, 'date_from' => date('Y-m'), 'date_to' => date('Y-m')]), ['title' => 'Таблица(интервал)', 'class' => 'btn btn-danger']);
                    },
                ],
            ],
        ],
    ]); ?>

</div>
