<?php


namespace app\helpers;


use app\models\db\User;

class AuthHelper
{
    public static function listRoles()
    {
        return [
            User::ROLE_USER => 'Пользователь',
            User::ROLE_ADMINISTRATOR => 'Администратор',
        ];
    }
}