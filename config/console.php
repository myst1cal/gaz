<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'weather'
    ],
    'controllerNamespace' => 'app\commands',
    'container' => [
        'definitions' => [
            'app\modules\weather\interfaces\IGeoPointsRepository' => [
                'class' => 'app\repositories\GeoPointRepository',
            ],
            'app\modules\weather\interfaces\IWeatherApi' => [
                'class' => 'app\modules\weather\api\OpenWeatherMapApi',
            ],
            'app\modules\weather\services\WeatherService' => [
                'class' => 'app\modules\weather\services\WeatherService',
            ],
            'app\modules\weather\repositories\ForecastRepository' => [
                'class' => 'app\modules\weather\repositories\ForecastRepository',
            ],
        ],
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'modules' => [
        'admin' => ['class' => 'app\modules\admin\Module'],
        'weather' => ['class' => 'app\modules\weather\Module'],
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'db' => $db,
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
