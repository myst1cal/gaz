<?php


namespace app\modules\admin\dto;


class ReportDto
{
    /** @var ReportProviderDto[] $providers */
    public $providers;

    /** @var float $totalQuantity */
    public $totalQuantity;

    /** @var string $date */
    public $date;
}