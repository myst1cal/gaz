<?php

use yii\db\Migration;

/**
 * Class m190220_162437_alterEgrpoColumnInUserTable
 */
class m190220_162437_alterEgrpoColumnInUserTable extends Migration
{
    public function safeUp()
    {
        $this->alterColumn(\app\models\db\User::tableName(), 'egrpo', $this->integer()->null()->unsigned());
    }

    public function safeDown()
    {
    }

}
