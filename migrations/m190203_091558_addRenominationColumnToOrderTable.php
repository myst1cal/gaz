<?php

use app\models\db\Order;
use yii\db\Migration;


class m190203_091558_addRenominationColumnToOrderTable extends Migration
{
    public function safeUp()
    {
        $this->renameColumn(Order::tableName(), 'quantity', 'nomination');

        $this->addColumn(Order::tableName(), 'renomination', $this->float()->defaultValue(0)->after('nomination'));
    }

    public function safeDown()
    {
        $this->renameColumn(Order::tableName(), 'nomination', 'quantity');

        $this->dropColumn(Order::tableName(), 'renomination');
    }
}
