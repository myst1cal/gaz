<?php

namespace app\modules\admin\helpers;


class DateHelper
{
    public static function getMonths()
    {
        $now = date('Y-m');

        $response = [];
        for ($year = 2019; ; $year++) {
            for ($month = 1; $month < 13; $month++) {
                $key = date('Y-m', strtotime($year . '-' . $month . '-1'));

                $response[$key] = $month . '.' . $year;

                if ($key == $now) {
                    return $response;
                }
            }
        }
    }
}