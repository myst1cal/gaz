<?php

use app\models\db\Order;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\db\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Уведомления', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="notice-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            [
                'label' => 'Счетчик',
                'value' => function ($model) {
                    /** @var Order $model */
                    return $model->meter->title;
                },
            ],
            'nomination',
            'renomination',
            [
                'label' => 'Ж/д',
                'value' => function ($model) {
                    /** @var Order $model */
                    return $model->railway->title ?: '';
                },
            ],
            'date:date',
        ],
    ]) ?>

</div>
