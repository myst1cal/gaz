<?php

use app\assets\UserDynamicPhoneAsset;
use app\models\db\User;
use app\modules\admin\models\forms\UserForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\User */
/* @var $model UserForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Редактирование пользователя: ' . $model->xcode;
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="user-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'egrpo')->textInput() ?>

        <div class="panel panel-default">
            <div class="panel-heading"><h4><i class="glyphicon glyphicon-pencil"></i> Ответственные лица</h4></div>
            <div class="panel-body">
                <div class="container-items">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'position1_full_name')->textInput() ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'position2_full_name')->textInput() ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'signatory')->dropDownList(User::getSignatoryList()) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Телефоны</h4></div>
            <div class="panel-body">
                <div class="container-items">
                    <?php foreach ($phones as $i => $phone): ?>
                        <div class="phone-item panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left">Телефон</h3>
                                <div class="pull-right">
                                    <button type="button" class="add-phone btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button type="button" class="remove-phone btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?= $form->field($phone, "[{$i}]number")->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($phone, "[{$i}]comment")->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <?php UserDynamicPhoneAsset::register($this) ?>
</div>
