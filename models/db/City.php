<?php

namespace app\models\db;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $name
 * @property double $lat
 * @property double $lon
 * @property int $deleted [smallint(6)]
 */
class City extends ActiveRecord
{
    public static function tableName()
    {
        return 'city';
    }

    public function rules()
    {
        return [
            [['name', 'lat', 'lon'], 'required'],
            [['lat', 'lon'], 'number'],
            [['deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'lat' => 'Широта',
            'lon' => 'Долгота',
            'deleted' => 'Удален',
        ];
    }
}
