<?php


namespace app\interfaces;


interface IOrder
{
    public function getZcode();
    public function getDate();
}