<?php

namespace app\modules\admin\models\searches;

use app\models\db\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\db\File;

/**
 * FileSearch represents the model behind the search form of `app\models\db\File`.
 */
class FileSearch extends File
{
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['type', 'upload_time', 'url', 'file_name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = File::find()->leftJoin(User::tableName(), 'user_id=user.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'id' => $this->id,
            'upload_time' => $this->upload_time,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'user.xcode', $this->user_id])
            ->andFilterWhere(['like', 'file_name', $this->file_name]);

        return $dataProvider;
    }
}
