<?php

namespace app\dto\order;


class UserXlsDataDto
{
    /** @var UserXlsDataRowDto[] $rows */
    public $rows;
}