<?php

namespace app\modules\weather\api;

use app\modules\weather\dto\WeatherForecastDto;
use app\modules\weather\dto\WeatherForecastsDto;
use app\modules\weather\interfaces\IGeoPoint;
use app\modules\weather\interfaces\IWeatherApi;

class OpenWeatherMapApi implements IWeatherApi
{
    const AUTH_KEY = '8214c37aea143c8d341376ee897eeec1';

    /**
     * @param IGeoPoint $geoPoint
     * @return WeatherForecastsDto
     */
    public function getForecast(IGeoPoint $geoPoint): WeatherForecastsDto
    {
        $contextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );

        $url = "https://api.openweathermap.org/data/2.5/forecast?cnt=40&units=metric&lat={$geoPoint->getLat()}&lon={$geoPoint->getLon()}&appid=" . self::AUTH_KEY;
        $data = file_get_contents($url, false, stream_context_create($contextOptions));

        $weatherForecastsDto = new WeatherForecastsDto();
        $forecasts = json_decode($data, true);

        foreach ($forecasts['list'] as $forecast) {
            $weatherForecastDto = new WeatherForecastDto();

            $weatherForecastDto->datetime = $forecast['dt_txt'];
            $weatherForecastDto->foreignId = $geoPoint->getId();
            $weatherForecastDto->lat = $geoPoint->getLat();
            $weatherForecastDto->lon = $geoPoint->getLon();
            $weatherForecastDto->temp_min = $forecast['main']['temp_min'];
            $weatherForecastDto->temp_avg = $forecast['main']['temp'];
            $weatherForecastDto->temp_max = $forecast['main']['temp_max'];

            $weatherForecastsDto->weatherForecasts[] = $weatherForecastDto;
        }

        return $weatherForecastsDto;
    }
}