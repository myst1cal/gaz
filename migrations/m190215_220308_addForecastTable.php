<?php

use yii\db\Migration;

/**
 * Class m190215_220308_addForecastTable
 */
class m190215_220308_addForecastTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%weather_forecast}}', [
            'id' => $this->primaryKey(),
            'datetime' => $this->dateTime()->notNull(),
            'foreign_id' => $this->integer()->null(),
            'lon' => $this->double()->notNull(),
            'lat' => $this->double()->notNull(),
            'temp_min' => $this->double()->notNull(),
            'temp_avg' => $this->double()->notNull(),
            'temp_max' => $this->double()->notNull(),
        ]);

        $this->createIndex(
            'weather_forecast_datetime_foreign_id',
            '{{%weather_forecast}}',
            ['datetime', 'foreign_id']
            );
    }

    public function safeDown()
    {
        $this->dropIndex('weather_forecast_datetime_foreign_id', '{{%weather_forecast}}');

        $this->dropTable('{{%weather_forecast}}');
    }
}
