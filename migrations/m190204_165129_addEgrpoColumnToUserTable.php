<?php

use app\models\db\User;
use yii\db\Migration;


class m190204_165129_addEgrpoColumnToUserTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(User::tableName(), 'egrpo', 'mediumint unsigned null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(User::tableName(), 'egrpo');
    }
}
