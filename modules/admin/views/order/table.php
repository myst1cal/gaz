<?php

use app\models\db\Meter;
use app\models\db\Order;
use app\services\OrderService;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $orders Order[] */
/* @var $meter Meter */
/* @var $nominationSum double */
/* @var $renominationSum double */
/* @var $days array */

$this->title = 'Заказ на месяц';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('К списку', ['user/view', 'id' => $meter->user->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Печать номинации',
            Url::current([], true), [
                'target'      => '_blank',
                'class'       => 'btn btn-info',
                'data-method' => 'POST',
                'data-params' => [
                    'print'      => true,
                    'nomination' => true,
                    'csrf_param' => Yii::$app->request->csrfParam,
                    'csrf_token' => Yii::$app->request->csrfToken,
                ],
            ]) ?>​
        <?= Html::a('Печать реноминации',
            Url::current([], true), [
                'target'      => '_blank',
                'class'       => 'btn btn-primary',
                'data-method' => 'POST',
                'data-params' => [
                    'print'        => true,
                    'renomination' => true,
                    'csrf_param'   => Yii::$app->request->csrfParam,
                    'csrf_token'   => Yii::$app->request->csrfToken,
                ],
            ]) ?>​
    </p>

    <h3>Номинация</h3>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Месяц</th>
            <th>Сумма</th>
            <?php foreach ($days as $day): ?>
                <th><?= $day ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?= $date ?></td>
            <td><?= $nominationSum ?></td>
            <?php foreach ($days as $day): ?>
                <td class="width40"><?= $orders[$day]->nomination ?? 0 ?></td>
            <?php endforeach; ?>
        </tr>
        </tbody>
    </table>

    <h3>Реноминация</h3>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Месяц</th>
            <th>Сумма</th>
            <?php foreach ($days as $day): ?>
                <th><?= $day ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?= $date ?></td>
            <td><?= $renominationSum ?></td>
            <?php foreach ($days as $day): ?>
                <td class="width40"><?=  $orders[$day]->renomination ?? $orders[$day]->nomination ?></td>
            <?php endforeach; ?>
        </tr>
        </tbody>
    </table>
</div>
