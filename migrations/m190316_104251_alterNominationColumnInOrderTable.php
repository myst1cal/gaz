<?php

use app\models\db\Order;
use yii\db\Migration;


class m190316_104251_alterNominationColumnInOrderTable extends Migration
{
    public function safeUp()
    {
        $this->alterColumn(Order::tableName(), 'nomination', $this->decimal(10, 3)->defaultValue(0));
        $this->alterColumn(Order::tableName(), 'renomination', $this->decimal(10, 3)->defaultValue(0));

    }

    public function safeDown()
    {
        $this->alterColumn(Order::tableName(), 'nomination', $this->float()->defaultValue(0));
        $this->alterColumn(Order::tableName(), 'renomination', $this->float()->defaultValue(0));
    }
}
