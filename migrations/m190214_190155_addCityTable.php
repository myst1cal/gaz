<?php

use yii\db\Migration;

/**
 * Class m190214_190155_addCityTable
 */
class m190214_190155_addCityTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%city}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'lat' => $this->double()->notNull(),
            'lon' => $this->double()->notNull(),
            'deleted' => $this->smallInteger()->notNull()->defaultValue(0),
        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%city}}');
    }
}
