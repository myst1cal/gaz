<?php

namespace app\modules\weather\interfaces;


interface IGeoPointsRepository
{
    /**
     * @return IGeoPoint[]
     */
    public function getAllPoints();
}