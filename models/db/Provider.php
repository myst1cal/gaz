<?php

namespace app\models\db;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "provider".
 *
 * @property int $id
 * @property string $title
 * @property int $deleted
 *
 * @property Meter[] $meters
 */
class Provider extends \yii\db\ActiveRecord
{
    const NOT_DELETED = 0;
    const DELETED = 1;

    public static function tableName()
    {
        return 'provider';
    }

    public static function getList()
    {
        return ArrayHelper::map(Provider::find()->all(), 'id', 'title');
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['deleted'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeters()
    {
        return $this->hasMany(Meter::className(), ['provider_id' => 'id']);
    }
}
