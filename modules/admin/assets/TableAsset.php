<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;


class TableAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets';

    public $css = [
    ];
    public $js = [
        'js/table.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
