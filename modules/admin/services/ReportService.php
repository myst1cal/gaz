<?php


namespace app\modules\admin\services;


use app\models\db\Meter;
use app\models\db\Order;
use app\models\db\Provider;
use app\modules\admin\dto\ReportDto;
use app\modules\admin\dto\ReportProviderDto;
use app\modules\admin\dto\ReportProviderItemDto;
use phpDocumentor\Reflection\Types\Self_;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use yii\db\Query;

class ReportService
{
    /**
     * @param $date
     * @return ReportDto
     */
    public static function getMonthReport($date)
    {
        $dateStart = $date . '-01';
        $dateEnd = date('Y-m-d', strtotime($dateStart . ' +1 month'));

        /** @var Provider[] $providers */
        $providers = Provider::find()
            ->rightJoin(Meter::tableName(), 'provider.id=meter.provider_id')
            ->rightJoin(Order::tableName(), 'meter.id=order.meter_id')
            ->where(['>=', 'order.date', $dateStart])
            ->andWhere(['<', 'order.date', $dateEnd])
            ->all();

        $monthReportDto = new ReportDto();
        $monthReportDto->date = $date;

        foreach ($providers as $provider) {
            $monthReportProviderDto = new ReportProviderDto();
            $monthReportProviderDto->title = $provider->title;

            $monthReportDto->providers[] = $monthReportProviderDto;

            $orders = (new Query())
                ->select(['SUM(IF(o.renomination is null, o.nomination, o.renomination)) as quantity', 'o.date', 'm.zcode', 'm.address', 'm.title', 'u.xcode'])
                ->from('order as o')
                ->leftJoin('meter as m', 'm.id=o.meter_id')
                ->leftJoin('user as u', 'm.user_id=u.id')
                ->where(['>=', 'o.date', $dateStart])
                ->andWhere(['<', 'o.date', $dateEnd])
                ->andWhere(['m.provider_id' => $provider->id])
                ->groupBy('o.meter_id')
                ->all();

            foreach ($orders as $order) {
                $monthReportProviderItemDto = new ReportProviderItemDto();
                $monthReportProviderItemDto->title = $order['title'];
                $monthReportProviderItemDto->address = $order['address'];
                $monthReportProviderItemDto->zcode = $order['zcode'];
                $monthReportProviderItemDto->xcode = $order['xcode'];
                $monthReportProviderItemDto->quantity = $order['quantity'];

                $monthReportProviderDto->items[] = $monthReportProviderItemDto;

                $monthReportDto->totalQuantity += $monthReportProviderItemDto->quantity;
                $monthReportProviderDto->totalQuantity += $monthReportProviderItemDto->quantity;
            }
        }

        return $monthReportDto;
    }

    public static function getDayReport($date)
    {
        /** @var Provider[] $providers */
        $providers = Provider::find()
            ->rightJoin(Meter::tableName(), 'provider.id=meter.provider_id')
            ->rightJoin(Order::tableName(), 'meter.id=order.meter_id')
            ->where(['=', 'order.date', $date])
            ->all();

        $dayReportDto = new ReportDto();
        $dayReportDto->date = $date;

        foreach ($providers as $provider) {
            $dayReportProviderDto = new ReportProviderDto();
            $dayReportProviderDto->title = $provider->title;

            $dayReportDto->providers[] = $dayReportProviderDto;

            $orders = (new Query())
                ->select(['SUM(IF(o.renomination is null, o.nomination, o.renomination)) as quantity', 'o.date', 'm.zcode', 'm.address', 'm.title', 'u.xcode'])
                ->from('order as o')
                ->leftJoin('meter as m', 'm.id=o.meter_id')
                ->leftJoin('user as u', 'm.user_id=u.id')
                ->where(['=', 'o.date', $date])
                ->andWhere(['m.provider_id' => $provider->id])
                ->groupBy('o.meter_id')
                ->all();

            foreach ($orders as $order) {
                $dayReportProviderItemDto = new ReportProviderItemDto();
                $dayReportProviderItemDto->title = $order['title'];
                $dayReportProviderItemDto->address = $order['address'];
                $dayReportProviderItemDto->zcode = $order['zcode'];
                $dayReportProviderItemDto->xcode = $order['xcode'];
                $dayReportProviderItemDto->quantity = $order['quantity'];

                $dayReportProviderDto->items[] = $dayReportProviderItemDto;

                $dayReportDto->totalQuantity += $dayReportProviderItemDto->quantity;
                $dayReportProviderDto->totalQuantity += $dayReportProviderItemDto->quantity;
            }
        }

        return $dayReportDto;
    }

    private static $font = [
        'font' => [
            'bold' => true,
            'size' => 12,
        ],
    ];

    /**
     * @param ReportDto $report
     * @return Spreadsheet
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public static function dayReportToXls(ReportDto $report)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getColumnDimension('A')->setWidth(25);
        $sheet->getColumnDimension('B')->setWidth(25);
        $sheet->getColumnDimension('C')->setWidth(25);
        $sheet->getColumnDimension('D')->setWidth(25);
        $sheet->getColumnDimension('E')->setWidth(25);

        $sheet->getStyle('A')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);

        $sheet->getStyle('A1:E1')->applyFromArray(self::$font)->getAlignment()->setWrapText(true);

        $sheet->setCellValue('A' . 1, 'Наименования подразделений (потребителей природного газа)');
        $sheet->setCellValue('B' . 1, 'Объем заказа, тыс. м3');
        $sheet->setCellValue('C' . 1, 'Адрес точки подключения');
        $sheet->setCellValue('D' . 1, 'EIC-код как субъекта рынка природного газа (символ X)');
        $sheet->setCellValue('E' . 1, 'EIC-код точки коммерческого учета субъекта (символ Z)');

        $sheet->getStyle('A2')->applyFromArray(self::$font);
        $sheet->setCellValue('A' . 2, 'Всего');
        $sheet->setCellValue('B' . 2, $report->totalQuantity);

        $rowNumber = 3;
        foreach ($report->providers as $provider) {
            $sheet->getStyle('A' . $rowNumber)->applyFromArray(self::$font);
            $sheet->setCellValue('A' . $rowNumber, $provider->title);

            $rowNumber++;

            foreach ($provider->items as $providerItem) {
                $sheet->setCellValue('A' . $rowNumber, $providerItem->title);
                $sheet->setCellValue('B' . $rowNumber, $providerItem->quantity);
                $sheet->setCellValue('C' . $rowNumber, $providerItem->address);
                $sheet->setCellValue('D' . $rowNumber, $providerItem->xcode);
                $sheet->setCellValue('E' . $rowNumber, $providerItem->zcode);

                $rowNumber++;
            }

            $sheet->getStyle('A' . $rowNumber)->applyFromArray(self::$font);
            $sheet->setCellValue('A' . $rowNumber, 'Всего по ' . $provider->title);
            $sheet->setCellValue('B' . $rowNumber, $provider->totalQuantity);

            $rowNumber++;
        }

        $sheet->getStyle('A1:A' . $rowNumber)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B1:B' . $rowNumber)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C1:C' . $rowNumber)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D1:D' . $rowNumber)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E1:E' . $rowNumber)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        return $spreadsheet;
    }

    /**
     * @param ReportDto $report
     * @return Spreadsheet
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public static function monthReportToXls(ReportDto $report)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getColumnDimension('A')->setWidth(25);
        $sheet->getColumnDimension('B')->setWidth(25);
        $sheet->getColumnDimension('C')->setWidth(25);
        $sheet->getColumnDimension('D')->setWidth(25);
        $sheet->getColumnDimension('E')->setWidth(25);

        $sheet->getStyle('A')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);

        $sheet->getStyle('A1:E1')->applyFromArray(self::$font)->getAlignment()->setWrapText(true);

        $sheet->setCellValue('A' . 1, 'Наименования подразделений (потребителей природного газа)');
        $sheet->setCellValue('B' . 1, 'Объем заказа, тыс. м3');
        $sheet->setCellValue('C' . 1, 'Адрес точки подключения');
        $sheet->setCellValue('D' . 1, 'EIC-код как субъекта рынка природного газа (символ X)');
        $sheet->setCellValue('E' . 1, 'EIC-код точки коммерческого учета субъекта (символ Z)');

        $sheet->getStyle('A2')->applyFromArray(self::$font);
        $sheet->setCellValue('A' . 2, 'Всего');
        $sheet->setCellValue('B' . 2, $report->totalQuantity);

        $rowNumber = 3;
        foreach ($report->providers as $provider) {
            $sheet->getStyle('A' . $rowNumber)->applyFromArray(self::$font);
            $sheet->setCellValue('A' . $rowNumber, $provider->title);

            $rowNumber++;

            foreach ($provider->items as $providerItem) {
                $sheet->setCellValue('A' . $rowNumber, $providerItem->title);
                $sheet->setCellValue('B' . $rowNumber, $providerItem->quantity);
                $sheet->setCellValue('C' . $rowNumber, $providerItem->address);
                $sheet->setCellValue('D' . $rowNumber, $providerItem->xcode);
                $sheet->setCellValue('E' . $rowNumber, $providerItem->zcode);

                $rowNumber++;
            }

            $sheet->getStyle('A' . $rowNumber)->applyFromArray(self::$font);
            $sheet->setCellValue('A' . $rowNumber, 'Всего по ' . $provider->title);
            $sheet->setCellValue('B' . $rowNumber, $provider->totalQuantity);

            $rowNumber++;
        }

        $sheet->getStyle('A1:A' . $rowNumber)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B1:B' . $rowNumber)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C1:C' . $rowNumber)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D1:D' . $rowNumber)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E1:E' . $rowNumber)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        return $spreadsheet;
    }
}