<?php

use yii\db\Migration;

/**
 * Class m190617_175109_alterUserTable
 */
class m190617_175109_alterUserTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'title', $this->string()->notNull());
        $this->addColumn('{{%user}}', 'position1', $this->string()->notNull());
        $this->addColumn('{{%user}}', 'position2', $this->string()->null());
        $this->addColumn('{{%user}}', 'position1_full_name', $this->string()->notNull());
        $this->addColumn('{{%user}}', 'position2_full_name', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'title');
        $this->dropColumn('{{%user}}', 'position1');
        $this->dropColumn('{{%user}}', 'position2');
        $this->dropColumn('{{%user}}', 'position1_full_name');
        $this->dropColumn('{{%user}}', 'position2_full_name');
    }
}
