<?php


namespace app\repositories;


use app\models\db\City;
use yii\helpers\ArrayHelper;

class CityRepository
{
    public function listCitiesIdName()
    {
        $cityList = City::find()->orderBy(['name' => SORT_ASC])->all();

        return ArrayHelper::map($cityList, 'id', 'name');
    }

}