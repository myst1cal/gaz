<?php

namespace app\dto\date;

class DateDto
{
    public $year;
    public $month;
    public $day;
}