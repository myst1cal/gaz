<?php

use app\models\db\User;
use yii\db\Migration;

/**
 * Class m191109_071445_addFieldsToUserTable
 */
class m191109_071445_addFieldsToUserTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn(User::tableName(), 'contract_number', $this->string(12)->null());
        $this->addColumn(User::tableName(), 'contract_conclusion_date', $this->date()->null());
    }

    public function safeDown()
    {
        $this->dropColumn(User::tableName(), 'contract_number');
        $this->dropColumn(User::tableName(), 'contract_conclusion_date');
    }
}
