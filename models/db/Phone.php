<?php

namespace app\models\db;

/**
 * This is the model class for table "phone".
 *
 * @property int $id
 * @property int $user_id
 * @property string $number
 * @property string $comment
 *
 * @property User $user
 */
class Phone extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'phone';
    }


    public function rules()
    {
        return [
            [['user_id', 'number'], 'required'],
            [['user_id'], 'integer'],
            [['number', 'comment'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'number' => 'Номер',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
