<?php


namespace app\validators;


use app\interfaces\IOrder;
use app\services\OrderService;
use yii\base\Model;
use yii\validators\Validator;

class OrderRenominationValidator extends Validator
{
    /**
     * @param IOrder|Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!OrderService::canRenominate($model)) {
            $this->addError($model, $attribute, 'Нельзя сделать реноминацию');
        }
    }
}