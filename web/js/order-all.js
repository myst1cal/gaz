$(function () {
    $('.submit-supply').on('click', function () {
        $('.to-excel').val(0);
        $('.form-all').submit();

        return true;
    });
    $('.submit-excel').on('click', function () {
        setTimeout(function () {
            $('.to-upload').removeClass('hide');
        }, 1000)

        $('.to-excel').val(1);
        $('.form-all').submit();

        return true;
    });
});