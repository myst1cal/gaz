$(function () {
    let myChart = Highcharts.chart('weather-forecast-chart', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Прогноз погоды'
        },
        yAxis: {
            title: {
                text: 'Температура (°C)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'минимум',
            lineWidth: 4,
            marker: {
                radius: 4
            }
        }, {
            name: 'максимум',
        }],
        credits: {
            enabled: false
        },
    });

    let getForecast = function () {
        let meterId = $('.meter').val();
        let url = $('.meter').attr('data-url');

        let csrfParam = $('[name="csrf-param"]').attr('content');
        let csrfToken = $('[name="csrf-token"]').attr('content');

        let data = {};
        data[csrfParam] = csrfToken;
        data['meterId'] = meterId;
        $.ajax({
            url: url,
            data: data,
            dataType: 'json',
            type: 'post',
            success: function (obj) {
                let data = JSON.parse(obj);

                myChart.xAxis[0].setCategories(data.categories, true);
                myChart.series[0].setData(data.minTemperaturePoints, true);
                myChart.series[1].setData(data.maxTemperaturePoints, true);
                myChart.redraw();
            }
        });
    };

    getForecast();

    $('.meter').on('change', function () {
        getForecast();
    });
});