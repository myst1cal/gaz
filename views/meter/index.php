<?php

use app\models\db\Meter;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\MeterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meter-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить счетчик', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Заказ', ['/order/all'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'options' => [
            'style'=>'overflow: auto; word-wrap: break-word;'
        ],
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'zcode',
            'title',
            'address:ntext',
            [
                'attribute' => 'provider_id',
                'value'     => function ($model) {
                    /** @var $model Meter */
                    return $model->provider->title ?? '';
                },
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{current} {next} {interval} {upload_file} {update}',
                'buttons'  => [
                    'interval' => function ($url, $model) {
                        return Html::a('Таблица(интервал)', Url::to(['order/table', 'meter_id' => $model->id, 'date_from' => date('Y-m'), 'date_to' => date('Y-m')]), ['title' => 'Таблица(интервал)', 'class' => 'btn btn-danger']);
                    },
                    'upload_file' => function ($url, $model) {
                        return Html::a('Выгрузка файла', Url::to(['meter/file-upload', 'meter_id' => $model->id]), ['title' => 'Выгрузка файла', 'class' => 'btn btn-primary']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
