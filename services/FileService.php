<?php


namespace app\services;


use app\dto\order\UserXlsDataDto;
use app\dto\order\UserXlsDataRowDto;
use app\helpers\DateHelper;
use app\models\db\File;
use app\dto\order\OrderFileDto;
use app\models\db\User;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use yii\db\Exception;
use yii\db\Expression;

class FileService
{
    const FILE_TYPE_STANDARD = 1;
    const FILE_TYPE_MORE_5_ZCODES = 2;

    /**
     * @param OrderFileDto $dto
     * @param User $user
     * @return File
     * @throws Exception
     */
    public static function createFromOrderFileUploadForm(OrderFileDto $dto, User $user)
    {
        $file = new File();

        $file->type = File::TYPE_ORDER;
        $file->user_id = $dto->userId;
        $file->upload_time = new Expression('NOW()');
        $file->url = $dto->url;
        $file->file_name = $dto->fileName;
        $file->file_name_encode = $dto->fileNameEncode;

        if ($file->save()) {
            return $file;
        }

        throw new Exception(implode(';', $file->getErrors()));
    }

    static private function isMore5ZcodesFile(Worksheet $worksheet)
    {
        $array = $array = $worksheet->toArray();
    }

    /**
     * @param OrderFileDto $orderFileDto
     * @return UserXlsDataDto
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public static function parse(OrderFileDto $orderFileDto)
    {
        $reader = new Xls();
        $spreadsheet = $reader->load($orderFileDto->url . $orderFileDto->fileName);

        $worksheet = $spreadsheet->getActiveSheet();

        $array = $worksheet->rangeToArray('A1:AJ10000');

        $userXlsDataDto = new UserXlsDataDto();

        if ($array[9][0] == 'РАЗОМ') {
            // шаблон больше 5 z-кодов
            $month = '';
            $year = '';
            for ($rowNumber = 10, $exit = true; $exit; ++$rowNumber) {
                if (is_null($array[$rowNumber][0])) {
                    $exit = false;
                    continue;
                }

                if (is_null($array[$rowNumber][4]) &&
                    is_null($array[$rowNumber][2]) &&
                    filter_var($array[$rowNumber][0], FILTER_VALIDATE_FLOAT)
                ) {
                    list($month, $year) = explode('.', $array[$rowNumber][0]);
                    continue;
                }

                $zcode = $array[$rowNumber][4];

                if (is_null($zcode)) {
                    continue;
                }

                for ($column = 5; $column < 36; ++$column) {
                    $nomination = $array[$rowNumber][$column];

                    if ($nomination) {
                        $userXlsDataRowDto = new UserXlsDataRowDto();

                        $day = $column - 4;
                        $date = "$year-$month-$day";

                        $userXlsDataRowDto->date = $date;
                        $userXlsDataRowDto->nomination = $nomination;
                        $userXlsDataRowDto->zcode = $zcode;

                        $userXlsDataDto->rows["$year-$month"][$zcode][] = $userXlsDataRowDto;
                    }
                }
            }
        } else {
            $zcode = $array[10][7];
            $year = explode('/', $array[12][8])[2];

            for ($rowNumber = 17, $exit = true; $exit; ++$rowNumber) {

                try {
                    $month = DateHelper::getMonthByUkrainian($array[$rowNumber][0]);
                } catch (\Throwable $throwable) {
                    $exit = false;
                    continue;
                }

                for ($column = 2; $column < 33; ++$column) {
                    $nomination = $array[$rowNumber][$column];

                    if ($nomination) {
                        $userXlsDataRowDto = new UserXlsDataRowDto();

                        $day = $column - 1;
                        $date = "$year-$month-$day";
                        $userXlsDataRowDto->date = $date;
                        $userXlsDataRowDto->nomination = $nomination;
                        $userXlsDataRowDto->zcode = $zcode;

                        $userXlsDataDto->rows["$year-$month"][$zcode][] = $userXlsDataRowDto;
                    }
                }
            }
        }

        return $userXlsDataDto;
    }
}