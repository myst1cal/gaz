<?php

namespace app\dto\order;


class UserXlsDataRowDto
{
    /** @var double $nomination */
    public $nomination;

    /** @var double $renomination */
    public $renomination;

    /** @var string $date */
    public $date;

    /** @var string $zcode */
    public $zcode;
}