<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Railway */

$this->title = 'Добавление Ж/Д';
$this->params['breadcrumbs'][] = ['label' => 'Ж/Д', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="railway-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
