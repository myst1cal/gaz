<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\NoticeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Уведомления';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notice-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'date',
            'message',
            [
                'attribute' => 'status',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return Html::checkbox('status_' . $model->id, $model->status, ['class' => 'form-control update-notice-status', 'data-id' => $model->id]);
                },
            ],
            [
                'label'  => '',
                'format' => 'raw',
                'value'  => function ($model) {
                    return Html::a('ссылка', ['notice/view-' . $model->model, 'id' => $model->foreign_key], ['class' => 'form-control', 'target' => '_blank']);
                },
            ],
        ],
    ]); ?>
</div>
