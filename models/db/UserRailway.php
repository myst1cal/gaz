<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "user_railway".
 *
 * @property int $user_id
 * @property int $railway_id
 *
 * @property Railway $railway
 * @property User $user
 */
class UserRailway extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user_railway';
    }

    public function rules()
    {
        return [
            [['user_id', 'railway_id'], 'required'],
            [['user_id', 'railway_id'], 'integer'],
            [['user_id', 'railway_id'], 'unique', 'targetAttribute' => ['user_id', 'railway_id']],
            [['railway_id'], 'exist', 'skipOnError' => true, 'targetClass' => Railway::className(), 'targetAttribute' => ['railway_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'railway_id' => 'Railway ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRailway()
    {
        return $this->hasOne(Railway::className(), ['id' => 'railway_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
