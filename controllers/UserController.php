<?php

namespace app\controllers;

use app\models\db\Phone;
use app\models\db\User;
use app\models\forms\UserForm;
use app\services\UserService;
use app\services\PhoneService;
use yii\filters\AccessControl;
use yii\web\Controller;


class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['info', 'update'],
                        'allow' => true,
                        'roles' => [User::ROLE_USER],
                    ],
                ],
            ],
        ];
    }

    public function actionInfo()
    {
        /** @var User $model */
        $user = \Yii::$app->user->identity;
        return $this->render('info', ['user' => $user]);
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        $userId = \Yii::$app->user->getId();
        $form = UserService::createUserFormById($userId);
        $form->scenario = UserForm::SCENARIO_UPDATE;

        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            if ($id = UserService::createFromUserForm($form)) {
                $phones = \Yii::$app->request->post('Phone');
                if (!empty($phones)) {
                    $phonesDto = PhoneService::createDtoFromPhonesAndUserId($phones, $userId);
                    PhoneService::updateUserPhones($phonesDto);
                }

                return $this->redirect(['info']);
            }
        }

        $phones = Phone::findAll(['user_id' => $userId]);
        if (empty($phones)) {
            $phones[] = new Phone();
        }
        return $this->render('update', [
            'model' => $form,
            'phones' => $phones,
        ]);
    }
}
