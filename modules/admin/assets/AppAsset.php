<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;


class AppAsset extends AssetBundle
{
//    public $basePath = '@webroot';
//    public $baseUrl = '@web';
    public $sourcePath = '@app/modules/admin/assets';

    public $css = [
        'css/admin-site.css',
    ];
    public $js = [
        'js/report.js',
        'js/notice.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
