<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\db\Meter */

$this->title = 'Выгрузка заказов по счетчику - ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);
?>
<div class="meter-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::beginForm('', 'post') ?>
    <div class="row">
        <div class="col-lg-3">
            <?= DatePicker::widget([
                'language' => 'ru',
                'separator' => 'до',
                'name' => 'date_from',
                'value' => date('Y-m-01'),
                'type' => DatePicker::TYPE_RANGE,
                'name2' => 'date_to',
                'value2' => date('Y-m-01', strtotime('+1 month')),
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
        </div>
        <div class="col-lg-1">
            <?= Html::submitButton('В excel', ['class' => 'btn btn-info']) ?>
        </div>
    </div>
    <?= Html::endForm() ?>
    </p>

</div>
