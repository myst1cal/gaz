<?php

namespace app\models\searches;

use app\models\db\Meter;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class MeterSearch extends Meter
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'provider_id', 'deleted'], 'integer'],
            [['zcode', 'title', 'address'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $userId = null)
    {
        $query = Meter::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'provider_id' => $this->provider_id,
            'deleted' => $this->deleted,
        ]);


        if ($userId) {
            $query->andFilterWhere(['user_id' => $userId]);
        }

        $query->andFilterWhere(['like', 'zcode', $this->zcode])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
