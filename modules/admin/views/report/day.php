<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\Provider */
/* @var $form yii\widgets\ActiveForm */
?>
<?php

/* @var $this yii\web\View */
/* @var $model \app\modules\admin\models\forms\MonthForm */
/* @var $report \app\modules\admin\dto\ReportDto */

$this->title = 'Дневной отчет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-day">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="report-form">

        <?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::to(['report/day']),'id' => 'date-form']); ?>

        <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::className(),
            [
                'language' => 'ru',
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]
        ) ?>

        <?= $form->field($model, 'toXls')->hiddenInput()->label(false); ?>

        <div class="form-group">
            <?= Html::button('Получить', ['class' => 'btn btn-success report-submit']) ?>
            <?= Html::button('В excel', ['class' => 'btn btn-info report-day-xls']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <?php if (!empty($report)): ?>
        <table class="table table-text-center">
            <thead>
            <tr>
                <th>Наименования подразделений (потребителей природного газа)</th>
                <th>Объем заказа, тыс. м3</th>
                <th>Адрес точки подключения</th>
                <th>EIC-код как субъекта рынка природного газа (символ X)</th>
                <th>EIC-код точки коммерческого учета субъекта (символ Z)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-bold">ВСЕГО</td>
                <td><?= $report->totalQuantity ?></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php if ($report->providers): ?>
                <?php foreach ($report->providers as $provider): ?>
                    <tr>
                        <td class="text-bold"><?= $provider->title ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php foreach ($provider->items as $item): ?>
                        <tr>
                            <td><?= $item->title ?></td>
                            <td><?= $item->quantity ?></td>
                            <td><?= $item->address ?></td>
                            <td><?= $item->xcode ?></td>
                            <td><?= $item->zcode ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td class="text-bold">Разом по <?= $provider->title ?></td>
                        <td><?= $provider->totalQuantity ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>

            </tbody>
        </table>
    <?php endif; ?>
</div>

