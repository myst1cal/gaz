<?php

use yii\db\Migration;


class m190208_130849_addFilesTable extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%file}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(50)->notNull(),
            'user_id' => $this->integer()->null(),
            'upload_time' => $this->dateTime(),
            'url' => $this->string()->notNull(),
            'file_name' => $this->string()->notNull(),
        ]);

        $this->createIndex(
            'file_user_id',
            '{{%file}}',
            'user_id'
        );

        $this->addForeignKey(
            'fk_file_user_id',
            '{{%file}}',
            'user_id',
            'user',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_file_user_id', '{{%file}}');

        $this->dropIndex('file_user_id', '{{%file}}');

        $this->dropTable('{{%file}}');
    }
}
