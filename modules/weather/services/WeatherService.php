<?php

namespace app\modules\weather\services;


use app\modules\weather\dto\GeoPointDto;
use app\modules\weather\interfaces\IGeoPoint;
use app\modules\weather\interfaces\IWeatherApi;
use app\modules\weather\repositories\ForecastRepository;

class WeatherService
{
    private $api;
    private $repository;

    public function __construct(IWeatherApi $api, ForecastRepository $repository)
    {
        $this->api = $api;
        $this->repository = $repository;
    }

    /**
     * @param IGeoPoint $geoPoint
     */
    public function refresh(IGeoPoint $geoPoint)
    {
        $forecasts = $this->api->getForecast($geoPoint);

        foreach ($forecasts->weatherForecasts as $forecast) {
            try {
                $this->repository->set($forecast);
            } catch (\Throwable $t) {
                \Yii::error($t->getMessage());
            }
        }
    }
}