$(function () {
    let counter = 100;

    $('body').on('click', '.add-phone', function () {
        let html = $('.phone-item').get(0).outerHTML;
        console.log(html);
        html = html.replace(/\[[0-9]+\]/gmi, "[" + counter +"]");
        html = html.replace(/-[0-9]+-/gmi, "-" + counter +"-");
        counter+=1;

        $('.phone-item:last').after(html);
    });

    $('body').on('click', '.remove-phone', function () {
        if ($('.phone-item').length > 1) {
            $(this).closest('.phone-item').remove();
        }
    });
});