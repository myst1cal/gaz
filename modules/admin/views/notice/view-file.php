<?php

use app\models\db\Order;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\db\File */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Уведомления', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="notice-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            [
                'label' => 'Название',
                'value' => function ($model) {
                    /** @var \app\models\db\File $model */
                    return $model->file_name;
                },
            ],
            [
                'label' => 'Файл',
                'value' => function ($model) {
                    /** @var \app\models\db\File $model */
                    return $model->file_name_encode;
                },
            ],
            'upload_time:datetime',
            [
                'label' => 'Заказы',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var \app\models\db\File $model */
                    $orders = Order::findAll(['file_id' => $model->id]);
                    if (empty($orders)) {
                        return '';
                    }

                    $html = '';
                    foreach ($orders as $order) {
                        $html .= 'z-код: ' . $order->getZcode() . '<br>' .
                                'x-код: ' . $order->getXcode() . '<br>' .
                                'Дата: ' . $order->date . '<br><br>';
                    }

                    return $html;
                },
            ],
        ],
    ]) ?>

</div>
