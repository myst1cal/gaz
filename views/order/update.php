<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Order */
/* @var $startDate string */
/* @var $meterList array */
/* @var $railwayList array */

$this->title = 'Редактирование заказа: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'startDate' => $startDate,
        'meterList' => $meterList,
        'railwayList' => $railwayList,
    ]) ?>

</div>
