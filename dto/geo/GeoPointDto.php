<?php

namespace app\dto\geo;


use app\models\db\City;
use app\modules\weather\interfaces\IGeoPoint;

class GeoPointDto implements IGeoPoint
{
    private $city;

    public function __construct(City $city)
    {
        $this->city = $city;
    }

    public function getLat()
    {
        return $this->city->lat;
    }

    public function getLon()
    {
        return $this->city->lon;
    }

    public function getId()
    {
        return $this->city->id;
    }
}