$(function () {
    $('.table-submit').click(function () {
        $('.table-form').attr('action', '/order/table');
        $('.table-form').attr('method', 'get');
        $('.table-form').submit();
    });

    $('.table-to-excel').click(function () {
        $('.table-form').attr('action', '/order/print');
        $('.table-form').attr('method', 'post');
        $('.table-form').submit();
    });

    $('.table-print').click(function () {
        $('.table-form').attr('action', '/order/print-table');
        $('.table-form').attr('target', '_blank');
        $('.table-form').attr('method', 'post');
        $('.table-form').submit();
    });

    $('input').on('keyup', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 39) {
            console.log(code);
            $(this).parent().next('td').find('input').focus();
        }

        if (code == 37) {
            console.log(code);
            $(this).parent().prev('td').find('input').focus();
        }

    });
});