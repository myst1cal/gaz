<?php

namespace app\modules\weather\dto;


class WeatherForecastsDto
{
    /**
     * @var WeatherForecastDto[] $weatherForecasts
     */
    public $weatherForecasts;
}