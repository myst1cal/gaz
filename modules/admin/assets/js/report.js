$(function () {
    $('.report-submit').click(function () {
        $('#monthform-toxls').val(0);
        $('#date-form').submit();
    });
    $('.report-month-xls').click(function () {
        $('#monthform-toxls').val(1);
        $('#date-form').submit();
    });
    $('.report-day-xls').click(function () {
        $('#dayform-toxls').val(1);
        $('#date-form').submit();
    });
});