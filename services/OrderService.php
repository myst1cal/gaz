<?php


namespace app\services;


use app\dto\date\DateDto;
use app\dto\order\UserXlsDataDto;
use app\dto\order\UserXlsDataRowDto;
use app\interfaces\IOrder;
use app\models\db\File;
use app\models\db\Meter;
use app\models\db\Notice;
use app\models\db\Order;
use app\models\db\OrderHistory;
use app\models\db\User;
use app\models\forms\OrderForm;
use app\modules\admin\services\NoticeService;
use yii\db\Expression;

class OrderService
{
    public static function getStartDate()
    {
        return time() % (3600 * 24) < 3600 * 12 ? date('Y-m-d') : date('Y-m-d', time() + 86400);
    }

    /**
     * Реноменировать можно с 15 до 21 перед днем поставки
     *
     * @param IOrder $order
     * @return bool
     */
    public static function canRenominate(IOrder $order)
    {
        $date = $order->getDate();
        $canRenominateDay15 = strtotime($date . ' 15:00 - 1 day');
        $canRenominateDay21 = strtotime($date . ' 21:00 - 1 day');
        $now = time();

        return $canRenominateDay15 < $now && $now < $canRenominateDay21;
    }

    /**
     * Реноменировать можно с 15 до 21 перед днем поставки
     *
     * @param IOrder $order
     * @return bool
     */
    public static function canRenominateInTable($order)
    {
        if ($order instanceof IOrder) {
            $date = $order->getDate();
        } else {
            $date = $order;
        }

        $now = time();
        $canRenominateDay15 = strtotime($date . ' 15:00 - 1 day');
        $canRenominateDay21 = strtotime($date . ' 21:00 - 1 day');

        return $canRenominateDay15 < $now && $now < $canRenominateDay21;

    }

    /**
     * Редактировать можно до 12 часов дня перед днем поставки
     *
     * @param IOrder $order
     * @return bool
     */
    public static function canUpdate(IOrder $order)
    {
        $date = $order->getDate();
        $canUpdateDay12 = strtotime($date . ' 12:00 - 1 day');
        $now = time();

        return $now < $canUpdateDay12;
    }

    /**
     * Редактировать можно до 12 часов дня перед днем поставки
     *
     * @param IOrder $order
     * @return bool
     */
    public static function canUpdateInTable($order)
    {
        if ($order instanceof IOrder) {
            $date = $order->getDate();
        } else {
            $date = $order;
        }

        $canUpdateDay12 = strtotime($date . ' 12:00 - 1 day');
        $now = time();

        return $now < $canUpdateDay12;
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param $meterId
     * @return UserXlsDataDto
     */
    public static function getUserXlsData($dateFrom, $dateTo, $meterId)
    {
        $orders = Order::find()
            ->leftJoin(Meter::tableName(), 'meter_id=meter.id')
            ->where(['>=', 'order.date', $dateFrom])
            ->andWhere(['<', 'order.date', $dateTo])
            ->andWhere(['meter_id' => $meterId])
            ->all();

        $userXlsDataDto = new UserXlsDataDto();

        foreach ($orders as $order) {
            $userXlsDataRowDto = new UserXlsDataRowDto();

            $userXlsDataRowDto->date = $order->date;
            $userXlsDataRowDto->nomination = $order->nomination;
            $userXlsDataRowDto->renomination = $order->renomination;
            $userXlsDataRowDto->zcode = $order->meter->zcode;

            $userXlsDataDto->rows[] = $userXlsDataRowDto;
        }

        return $userXlsDataDto;
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param $meterId
     * @return UserXlsDataDto
     */
    public static function getUserXlsDataGroupedByMonths($dateFrom, $dateTo, $meterId)
    {
        $orders = Order::find()
            ->leftJoin(Meter::tableName(), 'meter_id=meter.id')
            ->where(['>=', 'order.date', $dateFrom])
            ->andWhere(['<', 'order.date', $dateTo])
            ->andWhere(['meter_id' => $meterId])
            ->all();

        $userXlsDataDto = new UserXlsDataDto();

        foreach ($orders as $order) {

            $userXlsDataRowDto = new UserXlsDataRowDto();

            $userXlsDataRowDto->date = $order->date;
            $userXlsDataRowDto->nomination = $order->nomination;
            $userXlsDataRowDto->renomination = $order->renomination;
            $userXlsDataRowDto->zcode = $order->meter->zcode;

            list(, $month, $day) = explode('-', $userXlsDataRowDto->date);
            $userXlsDataDto->rows[(int)$month][(int)$day] = $userXlsDataRowDto;
        }

        return $userXlsDataDto;
    }

    /**
     * @param OrderForm $form
     * @return int
     * @throws \Exception
     */
    public static function createFromOrderForm(OrderForm $form)
    {
        $order = new Order();

        $order->meter_id = $form->meter;
        $order->nomination = $form->nomination;
        $order->renomination = $form->renomination;
        $order->railway_id = $form->railway;
        $order->date = $form->date;
        if ($order->save()) {
            return $order->id;
        }

        throw new \Exception($order->firstErrors);
    }

    /**
     * @param UserXlsDataDto $orderFileDto
     * @param File $file
     */
    public static function confirm(UserXlsDataDto $orderFileDto, File $file)
    {
        foreach ($orderFileDto->rows as $date => $zcodes) {
            foreach ($zcodes as $zcode => $rows) {
                $meter = Meter::findOne(['zcode' => $zcode]);

                $history = new OrderHistory();
                $history->file_id = $file->id;
                $history->meter_id = $meter->id;
                $history->date_time = time();

                foreach ($rows as $row) {
                    $orderQuery = Order::find()
                        ->leftJoin(Meter::tableName(), 'meter.id=order.meter_id')
                        ->where(['meter.zcode' => $row->zcode])
                        ->andWhere(['order.date' => $row->date])
                        ->andWhere([
                            'OR', ['order.nomination' => $row->nomination], ['order.renomination' => $row->nomination]
                        ]);

                    list(, , $day) = explode('-', $row->date);
                    $history->{'day_' . $day} = $row->nomination;

                    $order = $orderQuery->one();

                    if ($order) {

                        $orderDate = \DateTime::createFromFormat('Y-m-d', $order->date);
                        $nowDate = new \DateTime();

                        if ($orderDate > $nowDate) {
                            $order->file_id = $file->id;

                            if ($order->save()) {
                                $notice = new Notice();

                                $notice->status = Notice::NEW_NOTICE;
                                $notice->message = 'Загружен файл';
                                $notice->foreign_key = $file->id;
                                $notice->model = 'file';

                                if ($notice->save()) {
                                    NoticeService::sendEmail($notice);
                                }
                            }
                        }
                    }
                }

                $history->save();
            }
        }
    }

    public static function saveNominations($nominations, $meterId, DateDto $dateDto)
    {
        foreach ($nominations as $day => $nomination) {
            if (empty($nomination)) {
                continue;
            }

            $date = "{$dateDto->year}-{$dateDto->month}-$day";
            $saveOrder = Order::findOne(['meter_id' => $meterId, 'date' => $date]);
            if (!$saveOrder) {
                $saveOrder = new Order();
                $saveOrder->date = $date;
                $saveOrder->meter_id = $meterId;
            }

            $saveOrder->nomination = $nomination;

            $saveOrder->save();
        }
    }

    public static function saveRenominations(array $renominations, $meterId, DateDto $dateDto)
    {
        foreach ($renominations as $day => $renomination) {
            if (empty($renomination)) {
                continue;
            }

            $date = date('Y-m-' . $day);
            $saveOrder = Order::findOne(['meter_id' => $meterId, 'date' => $date]);

            $saveOrder->renomination = $renomination;

            $saveOrder->save();
        }
    }

    public static function getOrdersByDayAndSums(DateDto $date, $meterId)
    {
        /** @var Order[] $orders */
        $orders = Order::find()
            ->where(new Expression('CAST(MONTH(date) AS UNSIGNED) = ' . $date->month . ' AND YEAR(date) = ' . $date->year))
            ->andWhere(['meter_id' => $meterId])
            ->orderBy('date')
            ->all();

        $nominationSum = 0;
        $renominationSum = 0;
        $data = [];

        $days = date('t', strtotime("{$date->year}-{$date->month}-01"));
        foreach (range(1, $days) as $day) {
            $data['days'][$day] = "{$date->year}-{$date->month}-" . $day;
        }

        foreach ($orders as $order) {
            $nominationSum += $order->nomination;
            $renominationSum += (double)$order->renomination ?: $order->nomination;
            $data['days'][$order->getDay()] = $order;
        }

        $data['sums']['nomination'] = $nominationSum;
        $data['sums']['renomination'] = $renominationSum;
        $data['daysInMonth'] = $days;

        foreach (range(1, $data['days']) as $day => $value) {
            $data['print']['days'][$day] = 0;
        }

        foreach ($orders as $order) {
            $data['print']['days'][$order->getDay()] = \Yii::$app->request->post('nomination') ? $order->nomination ?: 0 : ((double)$order->renomination ?: $order->nomination ?: 0);
        }

        return $data;
    }
}