<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Order */
/* @var $startDate string */
/* @var $meterList array */
/* @var $railwayList array */

$this->title = 'Создать заказ';
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'startDate' => $startDate,
        'railwayList' => $railwayList,
        'meterList' => $meterList,
    ]) ?>

</div>
