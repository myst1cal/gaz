<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\db\User */
/* @var $startDate string */
/* @var $meterList array */
/* @var $railwayList array */

$this->title = 'Пользователь: ' . $user->xcode;
$this->params['breadcrumbs'][] = 'Инфо';
?>
<div class="order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= \yii\widgets\DetailView::widget([
        'model' => $user,
        'attributes' => [
            'xcode',
            'email',
            'created_at:datetime',
            'updated_at:datetime',

        ],
    ]) ?>

</div>
