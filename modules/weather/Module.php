<?php

namespace app\modules\weather;

use yii\base\BootstrapInterface;

/**
 * weather module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = 'app\modules\weather\controllers';

    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        if ($app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'app\modules\weather\commands';
        }
    }
}
