<?php

namespace app\models\db;

/**
 * This is the model class for table "file".
 *
 * @property int $id
 * @property string $type
 * @property int $user_id
 * @property string $upload_time
 * @property string $url
 * @property string $file_name
 * @property string $file_name_encode [varchar(255)]
 * @property string $encode_public_key
 *
 * @property string $typeName
 * @property User $user
 */
class File extends \yii\db\ActiveRecord
{
    const TYPE_ORDER = 'order';

    public static function tableName()
    {
        return 'file';
    }

    public function rules()
    {
        return [
            [['type', 'url', 'file_name'], 'required'],
            [['user_id'], 'integer'],
            [['upload_time'], 'safe'],
            [['type'], 'string', 'max' => 50],
            [['encode_public_key'], 'string'],
            [['url', 'file_name', 'file_name_encode'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function getTypeName() {
        try {
            return File::listTypes()[$this->type];
        } catch (\Throwable $throwable) {
            return '';
        }
    }

    public static function listTypes()
    {
        return [
            File::TYPE_ORDER => 'Заказ',
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'user_id' => 'Владелец',
            'upload_time' => 'Дата время',
            'url' => 'Путь',
            'file_name' => 'Файл',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
