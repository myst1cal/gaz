<?php

namespace app\services;


use app\models\db\User;

class AuthService
{
    /**
     * @param User $user
     * @param string $roleName
     * @return bool
     */
    public static function hasRole(User $user, $roleName)
    {
        return \Yii::$app->authManager->getAssignment($roleName, $user->getId()) ? true : false;
    }
}