<?php

use yii\db\Migration;

/**
 * Class m190328_071331_updateFileEncryptPublicKeyColumnToUserTable
 */
class m190328_071331_updateFileEncryptPublicKeyColumnToUserTable extends Migration
{
    public function safeUp()
    {
        $this->alterColumn(\app\models\db\User::tableName(), 'file_encode_public_key', $this->text()->null());
    }

    public function safeDown()
    {
        $this->alterColumn(\app\models\db\User::tableName(), 'file_encode_public_key', $this->text()->notNull());
    }
}
