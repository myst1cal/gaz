<?php

use app\models\db\User;
use yii\db\Migration;

/**
 * Class m200630_064254_user_is_railway
 */
class m200630_064254_user_is_railway extends Migration
{
    public function safeUp()
    {
        $this->addColumn(User::tableName(), 'is_railway', $this->smallInteger()->defaultValue(User::NOT_RAILWAY)->comment('Может ли у пользователя быть жд'));
    }

    public function safeDown()
    {
        $this->dropColumn(User::tableName(), 'is_railway');
    }
}
