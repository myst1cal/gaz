<?php

use yii\db\Migration;


class m190203_131415_addRailwayTable extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%railway}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->unique(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%railway}}');
    }
}
