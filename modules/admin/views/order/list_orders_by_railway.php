<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var \app\models\db\Railway $railway */

$this->title = 'Заказы по ж/д ' . $railway->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provider-list_orders_by_provider">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Счетчик',
                'value' => function($model){
                    /** @var \app\models\db\Order $model */
                    return $model->meter->title ;
                }
            ],
            'nomination',
            'renomination',
            'date',
        ],
    ]); ?>
</div>
