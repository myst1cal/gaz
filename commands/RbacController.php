<?php

namespace app\commands;

use app\models\db\User;
use app\rules\MeterAccess;
use app\rules\OrderAccess;
use yii\console\Controller;
use yii\rbac\DbManager;

class RbacController extends Controller
{
    public function actionInit()
    {
        /** @var DbManager $authManager */
        $authManager = \Yii::$app->authManager;

        // Create roles
        $user = $authManager->createRole(User::ROLE_USER);
        //$moderator = $authManager->createRole('moderator');
        $admin = $authManager->createRole(User::ROLE_ADMINISTRATOR);

        //$guest->ruleName  = 'guest';
        //$user->ruleName  = 'user';
        //$moderator->ruleName = 'moderator';
        //$admin->ruleName = 'administrator';

        // Add roles in Yii::$app->authManager
        $authManager->add($user);
        //$authManager->add($moderator);
        $authManager->add($admin);

        $user = User::findOne(['xcode' => 'admin']);
        $role = $authManager->getRole(User::ROLE_ADMINISTRATOR);
        $authManager->assign($role, $user->id);
    }

    public function actionOrderAndMeterAccess()
    {
        /** @var DbManager $authManager */
        $authManager = \Yii::$app->authManager;

        $authManager->removeAllPermissions();
        $authManager->removeAllRules();

        $meterAccessRule = new MeterAccess();
        $orderAccessRule = new OrderAccess();

        $meterAccessRule->name = MeterAccess::className();
        $orderAccessRule->name = OrderAccess::className();

        $authManager->add($meterAccessRule);
        $authManager->add($orderAccessRule);

        $meterPermission = $authManager->createPermission('meterAccess');
        $meterPermission->description = 'Права на счетчик';
        $meterPermission->ruleName = $meterAccessRule->name;

        $orderPermission = $authManager->createPermission('orderAccess');
        $orderPermission->description = 'Права на заказ';
        $orderPermission->ruleName = $orderAccessRule->name;

        $authManager->add($meterPermission);
        $authManager->add($orderPermission);

        $user = $authManager->getRole(User::ROLE_USER);

        $authManager->addChild($user, $meterPermission);
        $authManager->addChild($user, $orderPermission);
    }
}