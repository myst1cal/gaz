<?php

namespace app\models\forms;


use yii\base\Model;
use yii\web\UploadedFile;

class OrderFileUploadForm extends Model
{
    /** @var UploadedFile $orderFileEncode */
    public $orderFileEncode;

    public function rules()
    {
        return [
            [['orderFileEncode'], 'file', 'skipOnEmpty' => false],
            [['orderFileEncode'], 'validateP7s'],
        ];
    }

    public function validateP7s($attribute, $params, $validator)
    {
        if ($this->$attribute->extension != 'p7s') {
            $this->addError($attribute, 'Неверный формат файла (p7s)');
        }
    }

    public function attributeLabels()
    {
        return [
            'orderFileEncode' => 'Подтвержденный документ',
        ];
    }
}