<?php


namespace app\modules\admin\services;


use app\models\db\Notice;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class NoticeService
{
    public static function hasNotice()
    {
        $notice = Notice::findOne(['status' => Notice::NEW_NOTICE]);

        if ($notice) {
            return '<span class="btn btn-sm btn-danger"></span>';
        }

        return '';
    }

    public static function sendEmail(Notice $notice)
    {
        Yii::$app->mailer->compose()
            ->setHtmlBody($notice->message . ' ' . Html::a($notice->id, Url::to(['notice/view-' . $notice->model, 'id' => $notice->foreign_key], true)))
            ->setSubject('Новое уведомление')
            ->setTo(Yii::$app->params['noticeEmail'])
            ->send();
    }
}