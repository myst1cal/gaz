<?php

namespace app\modules\weather\repositories;


use app\modules\weather\dto\WeatherForecastDto;
use app\modules\weather\dto\WeatherForecastsDto;
use app\modules\weather\models\db\WeatherForecast;

class ForecastRepository
{
    /**
     * @param WeatherForecastDto $weatherForecast
     * @return bool
     * @throws \Exception
     */
    public function set(WeatherForecastDto $weatherForecast)
    {
        $dbWeatherForecast = WeatherForecast::findOne(['datetime' => $weatherForecast->datetime, 'foreign_id' => $weatherForecast->foreignId]);
        if (empty($dbWeatherForecast)) {
            $dbWeatherForecast = new WeatherForecast();
        }

        $dbWeatherForecast->datetime = $weatherForecast->datetime;
        $dbWeatherForecast->foreign_id = $weatherForecast->foreignId;
        $dbWeatherForecast->lon = $weatherForecast->lon;
        $dbWeatherForecast->lat = $weatherForecast->lat;
        $dbWeatherForecast->temp_min = $weatherForecast->temp_min;
        $dbWeatherForecast->temp_avg = $weatherForecast->temp_avg;
        $dbWeatherForecast->temp_max = $weatherForecast->temp_max;

        if ($dbWeatherForecast->save()) {
            return true;
        }

        throw new \Exception(implode('; ', $dbWeatherForecast->firstErrors));
    }

    public function getByForeignId(int $id)
    {
        $dbWeatherForecasts =  WeatherForecast::find()
            ->where(['foreign_id' => $id])
            ->andWhere(['>=', 'datetime', date("Y-m-d 00:00:00")])
            ->orderBy(['datetime' => SORT_ASC])
            ->all();

        $weatherForecasts = new WeatherForecastsDto();

        foreach ($dbWeatherForecasts as $dbWeatherForecast) {
            $weatherForecast = new WeatherForecastDto();

            $weatherForecast->id = $dbWeatherForecast->id;
            $weatherForecast->datetime = $dbWeatherForecast->datetime;
            $weatherForecast->foreignId = $id;
            $weatherForecast->lon = $dbWeatherForecast->lon;
            $weatherForecast->lat = $dbWeatherForecast->lat;
            $weatherForecast->temp_min = $dbWeatherForecast->temp_min;
            $weatherForecast->temp_avg = $dbWeatherForecast->temp_avg;
            $weatherForecast->temp_max = $dbWeatherForecast->temp_max;

            $weatherForecasts->weatherForecasts[] = $weatherForecast;
        }

        return $weatherForecasts;
    }
}