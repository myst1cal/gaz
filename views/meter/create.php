<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Meter */

$this->title = 'Добавление счетчика';
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meter-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cityRepository' => $cityRepository,
    ]) ?>

</div>
