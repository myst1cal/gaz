<?php

use app\models\db\User;
use yii\db\Migration;

/**
 * Class m190126_172640_addZcodeTable
 */
class m190126_172630_addProviderTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%provider}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->unique(),
            'deleted' => $this->smallInteger()->defaultValue(User::NOT_DELETED)
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%provider}}');
    }
}
