<?php

namespace app\models\db;

use app\interfaces\IOrder;
use app\modules\admin\services\NoticeService;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $meter_id
 * @property float $nomination
 * @property float $renomination
 * @property string $date
 * @property int $deleted
 * @property int $railway_id
 * @property int $file_id [int(11)]
 *
 * @property Railway $railway
 * @property File $file
 * @property string $zcode
 * @property string $xcode
 * @property Meter $meter
 */
class Order extends ActiveRecord implements IOrder
{
    const NOT_DELETED = 0;
    const DELETED = 1;

    const SCENARIO_RENOMINATE = 'renominate';

    public static function tableName()
    {
        return 'order';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_RENOMINATE] = ['renomination'];
        return $scenarios;
    }

    public function rules()
    {
        return [
            [['meter_id', 'nomination', 'date'], 'required'],
            [['meter_id', 'file_id', 'deleted'], 'integer'],
            [['nomination', 'renomination'], 'number'],
            [['meter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meter::className(), 'targetAttribute' => ['meter_id' => 'id']],
        ];
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if (!empty($this->nomination)) {
                $this->nomination = str_replace(',', '.', $this->nomination);
            }
            if (!empty($this->renomination)) {
                $this->renomination = str_replace(',', '.', $this->renomination);
            }

            return true;
        }
        return false;
    }

    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'meter_id'     => 'Счетчик',
            'nomination'   => 'Номинация, тыс. м3',
            'renomination' => 'Реноминация, тыс. м3',
            'date'         => 'Дата поставки',
            'deleted'      => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeter()
    {
        return $this->hasOne(Meter::className(), ['id' => 'meter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRailway()
    {
        return $this->hasOne(Railway::className(), ['id' => 'railway_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getZcode()
    {
        if (isset($this->meter)) {
            return $this->meter->zcode;
        }

        return '';
    }

    public function getXcode()
    {
        if (isset($this->meter->user)) {
            return $this->meter->user->xcode;
        }

        return '';
    }

    public function getDay()
    {
        return date('j', strtotime($this->date));
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $notice = new Notice();

        $notice->status = Notice::NEW_NOTICE;
        $notice->message = $insert ? 'Добавлен новый заказ' : 'Редактирование заказа';
        $notice->foreign_key = $this->id;
        $notice->model = 'order';

        if ($notice->save()) {
            NoticeService::sendEmail($notice);
        }
    }
}
