<?php


namespace app\modules\admin\dto;


class ReportProviderItemDto
{
    /** @var string $title */
    public $title;

    /** @var double $quantity */
    public $quantity;

    /** @var string $address */
    public $address;

    /** @var string $xcode */
    public $xcode;

    /** @var string $zcode */
    public $zcode;
}