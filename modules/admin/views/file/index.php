<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\searches\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Файлы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'type',
                'value' => function ($model) {
                    /** @var \app\models\db\File $model */

                    return $model->getTypeName();
                },
                'filter' => \app\models\db\File::listTypes(),
            ],
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    /** @var \app\models\db\File $model */

                    return $model->user->xcode ?: '';
                },
            ],
            [
                'attribute' => 'upload_time',
                'filter' => false,
            ],
            [
                'attribute' => 'file_name',
                'filter' => false,
            ],
            //[
            //    'label' => 'Ключ',
            //    'format' => 'raw',
            //    'value' => function ($model) {
            //        /** @var \app\models\db\File $model */

            //       return $model->encode_public_key;
            //    }
            //],
            [
                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var \app\models\db\File $model */
                    $html = Html::a('', '/' . $model->url . $model->file_name, ['title' => 'Файл', 'download' => true, 'class' => 'glyphicon glyphicon-download']);
                    $html .= Html::a('', '/' . $model->url . $model->file_name_encode, ['title' => 'Подписанный файл', 'download' => true, 'class' => 'glyphicon glyphicon-download']);

                    return $html;
                }
            ],
        ],
    ]); ?>
</div>
