<?php

namespace app\modules\weather\interfaces;


use app\modules\weather\dto\WeatherForecastsDto;

interface IWeatherApi
{
    public function getForecast(IGeoPoint $geoPoint): WeatherForecastsDto;
}