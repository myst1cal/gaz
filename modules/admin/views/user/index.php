<?php

use app\models\db\User;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\searches\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Название организации',
                'value' => function($model) {
                    /** @var User $model */

                    if ($model->is_railway) {
                        $titles = ArrayHelper::getColumn($model->railways, 'title');
                        $response = implode(',', $titles);
                    } else {
                        $response = $model->title;
                    }

                    return $response;
                }
            ],
            'xcode',
            'email',
            'egrpo',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'is_railway',
                'value' => function($model) {
                    /** @var User $model */
                    return $model->is_railway ? 'Да' : 'Нет';
                },
                'filter' => [1 => 'Да', 0 => 'Нет']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}'
            ],
        ],
    ]); ?>
</div>
