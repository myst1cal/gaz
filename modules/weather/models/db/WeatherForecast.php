<?php

namespace app\modules\weather\models\db;

use Yii;

/**
 * This is the model class for table "weather_forecast".
 *
 * @property int $id
 * @property string $datetime
 * @property int $foreign_id
 * @property double $lon
 * @property double $lat
 * @property double $temp_min
 * @property double $temp_avg
 * @property double $temp_max
 */
class WeatherForecast extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'weather_forecast';
    }

    public function rules()
    {
        return [
            [['datetime', 'lon', 'lat', 'temp_min', 'temp_avg', 'temp_max'], 'required'],
            [['datetime'], 'safe'],
            [['foreign_id'], 'integer'],
            [['lon', 'lat', 'temp_min', 'temp_avg', 'temp_max'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datetime' => 'Datetime',
            'foreign_id' => 'Foreign ID',
            'lon' => 'Lon',
            'lat' => 'Lat',
            'temp_min' => 'Temp Min',
            'temp_avg' => 'Temp Avg',
            'temp_max' => 'Temp Max',
        ];
    }
}
