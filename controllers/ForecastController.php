<?php

namespace app\controllers;

use app\models\db\Meter;
use app\models\db\User;
use app\modules\weather\repositories\ForecastRepository;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

class ForecastController extends Controller
{

    public $forecastRepository;

    public function __construct($id, $module, ForecastRepository $forecastRepository)
    {
        parent::__construct($id, $module, []);

        $this->forecastRepository = $forecastRepository;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['chart-by-meter-id'],
                        'allow' => true,
                        'roles' => [User::ROLE_USER, User::ROLE_ADMINISTRATOR],
                    ],
                ],
            ],
        ];
    }

    public function actionChartByMeterId()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $meterId = Yii::$app->request->post('meterId');
        $meter = Meter::findOne($meterId);

        if ($meter->city_id) {
            $forecasts = $this->forecastRepository->getByForeignId($meter->city_id);

            $categories = ArrayHelper::getColumn($forecasts->weatherForecasts, 'datetime');
            $minTemperaturePoints = ArrayHelper::getColumn($forecasts->weatherForecasts, 'temp_min');
            $maxTemperaturePoints = ArrayHelper::getColumn($forecasts->weatherForecasts, 'temp_max');

            $categories = str_replace('00:00', '00', $categories);

            foreach ($minTemperaturePoints as &$minTemperaturePoint) {
                $minTemperaturePoint = floor($minTemperaturePoint);
            }

            foreach ($maxTemperaturePoints as &$maxTemperaturePoint) {
                $maxTemperaturePoint = floor($maxTemperaturePoint);
            }

            return json_encode([
                'categories' => $categories,
                'maxTemperaturePoints' => $maxTemperaturePoints,
                'minTemperaturePoints' => $minTemperaturePoints
            ]);
        }

        return json_encode([]);
    }
}
