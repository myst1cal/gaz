<?php

namespace app\models\forms;

use app\interfaces\IOrder;
use app\models\db\Meter;
use app\validators\OrderDateValidator;
use app\validators\OrderRenominationValidator;
use yii\base\Model;


/**
 *
 * @property string $zcode
 */
class OrderForm extends Model implements IOrder
{
    public $id;
    public $meter;
    public $railway;
    public $date;
    public $nomination;
    public $renomination;


    public function rules()
    {
        return [
            ['id', 'trim'],
            ['id', 'integer'],

            ['meter', 'trim'],
            ['meter', 'integer'],

            ['railway', 'trim'],
            ['railway', 'integer'],

            ['date', OrderDateValidator::className()],

            ['nomination', 'trim'],
            ['nomination', 'required'],
            ['nomination', 'number'],

            ['renomination', 'trim'],
            ['renomination', 'number'],
            ['renomination', OrderRenominationValidator::className()],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meter' => 'Счетчик',
            'railway' => 'Ж/д',
            'nomination' => 'Номинация, тыс. м3',
            'renomination' => 'Реноминация, тыс. м3',
            'date' => 'Дата поставки',
        ];
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getZcode()
    {
        return Meter::findOne($this->meter)->zcode;
    }
}
