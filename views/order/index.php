<?php

use app\assets\OrderAsset;
use app\models\db\Order;
use app\services\OrderService;
use yii\grid\GridView;
use yii\helpers\Html;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\OrderSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $startDate string */

/* @var $meterList array */
/* @var $railwayList array */

/* @var $categories array */
/* @var $minTemperaturePoints array */
/* @var $maxTemperaturePoints array */
$this->title = 'Погода';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="order-form">
        <div class="row">
            <div class="col-lg-12">
                <?= Html::dropDownList('meter', null, $meterList, [
                    'class' => 'form-control meter', 'data-url' => Url::to('/forecast/chart-by-meter-id')
                ]) ?>
            </div>
            <div class="col-lg-12">
                <div id="weather-forecast-chart" class="weather-forecast-chart"></div>
            </div>
        </div>
    </div>
    <?php
    OrderAsset::register($this);
    ?>
</div>
