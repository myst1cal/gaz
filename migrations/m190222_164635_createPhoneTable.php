<?php

use yii\db\Migration;

class m190222_164635_createPhoneTable extends Migration
{

    public function safeUp()
    {
        $this->createTable('phone', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'number' => $this->string()->notNull(),
            'comment' => $this->string()->null()
        ]);

        $this->createIndex(
            'phone_user_id',
            'phone',
            'user_id'
        );

        $this->addForeignKey(
            'fk_phone_user_id',
            'phone',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }


    public function safeDown()
    {
        $this->dropForeignKey('fk_phone_user_id', 'phone');
        $this->dropIndex('phone_user_id', 'phone');
        $this->dropTable('phone');
    }
}
