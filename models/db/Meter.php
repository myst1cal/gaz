<?php

namespace app\models\db;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "meter".
 *
 * @property int $id
 * @property int $user_id
 * @property int $provider_id
 * @property string $zcode
 * @property string $title
 * @property string $address
 * @property int $deleted
 * @property int $city_id [int(11)]
 *
 * @property Provider $provider
 * @property User $user
 * @property City $city
 */
class Meter extends ActiveRecord
{
    const NOT_DELETED = 0;
    const DELETED = 1;

    public static function tableName()
    {
        return 'meter';
    }

    public function rules()
    {
        return [
            [['user_id', 'provider_id', 'zcode', 'title', 'address'], 'required'],
            [['user_id', 'provider_id', 'city_id', 'deleted'], 'integer'],
            [['address'], 'string'],
            [['zcode', 'title'], 'string', 'max' => 255],
            [['zcode'], 'unique'],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Provider::className(), 'targetAttribute' => ['provider_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'provider_id' => 'Поставщик',
            'city_id' => 'Город',
            'zcode' => 'Z - код',
            'title' => 'Название',
            'address' => 'Адрес',
            'deleted' => 'Удален',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Provider::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
}
