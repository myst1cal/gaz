<?php


namespace app\repositories;


use app\models\db\Order;

class OrderRepository
{
    /**
     * @param int $id
     * @return Order
     */
    public static function getById(int $id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new \ExceptionException('Запрашиваемый заказ не найден.');
    }
}