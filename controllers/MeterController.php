<?php

namespace app\controllers;

use app\dto\order\UserXlsDataRowDto;
use app\helpers\DateHelper;
use app\models\db\Meter;
use app\models\db\User;
use app\models\searches\MeterSearch;
use app\repositories\CityRepository;
use app\repositories\MeterRepository;
use app\services\OrderService;
use Codeception\PHPUnit\ResultPrinter\UI;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;


class MeterController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_USER],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new MeterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->getId());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        if (Yii::$app->user->can('meterAccess', ['meterId' => $id])) {
            $meter = MeterRepository::getById($id);
            return $this->render('view', [
                'model' => $meter,
            ]);
        }
    }

    public function actionCreate()
    {
        $model = new Meter();
        $model->user_id = Yii::$app->user->getId();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $cityRepository = new CityRepository();

        return $this->render('create', [
            'model' => $model,
            'cityRepository' => $cityRepository,
        ]);
    }

    public function actionUpdate($id)
    {
        if (Yii::$app->user->can('meterAccess', ['meterId' => $id])) {
            $model = MeterRepository::getById($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            $cityRepository = new CityRepository();

            return $this->render('update', [
                'model' => $model,
                'cityRepository' => $cityRepository,
            ]);
        }
    }

    public function actionFileUpload()
    {
        $meterId = Yii::$app->request->get('meter_id');

        if (Yii::$app->user->can('meterAccess', ['meterId' => $meterId])) {
            $meter = MeterRepository::getById($meterId);

            $dateFrom = Yii::$app->request->post('date_from');
            $dateTo = Yii::$app->request->post('date_to');

            if ($dateFrom && $dateTo) {
                if (strlen($dateFrom) == 7) {
                    $dateFrom = $dateFrom . '-01';
                    $daysInMonth = date('t', strtotime($dateTo . '-01'));
                    $dateTo = $dateTo . '-' . $daysInMonth;
                }

                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                $spreadsheet = $reader->load("upload_file_templates/application_order_standard.xls");

                try {
                    $sheet = $spreadsheet->getActiveSheet();

                    $data = OrderService::getUserXlsDataGroupedByMonths($dateFrom, $dateTo, $meterId);


                    $row = 18;
                    foreach ($data->rows as $month => $days) {
                        $perMonth = 0;

                        /**
                         * @var integer $day
                         * @var UserXlsDataRowDto $dto
                         */
                        foreach ($days as $day => $dto) {
                            $perMonth += $dto->nomination ?: $dto->renomination;
                            $sheet->setCellValueByColumnAndRow(2 + $day, $row, $dto->nomination ?: $dto->renomination);
                        }

                        $sheet->setCellValueByColumnAndRow(1, $row, DateHelper::getUkrainianMonth($month));
                        $sheet->setCellValueByColumnAndRow(2, $row, $perMonth);

                        ++$row;

                        if ($row - 18 < count($data->rows)) {
                            $sheet->insertNewRowBefore($row);
                        }
                    }

                    /** @var User $user */
                    $user = Yii::$app->user->getIdentity();

                    $sheet->setCellValue('H11', $meter->zcode);
                    $sheet->setCellValue('I13', (new \DateTime($dateFrom))->format('d/m/Y'));
                    $sheet->setCellValue('I14', (new \DateTime($dateTo))->format('d/m/Y'));
                    $sheet->setCellValue('D2', '__.__.' . (new \DateTime($dateFrom))->format('Y'));
                    $sheet->setCellValue('Z2', '__.__.' . (new \DateTime($dateFrom))->format('Y'));
                    $sheet->setCellValue('B' . ($row + 8), $user->signatoryName());
                    $sheet->setCellValue('H' . ($row + 10), $user->signatoryUserName());
                    $sheet->getStyle('H' . ($row + 10))->applyFromArray([
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_LEFT,
                        ]
                    ]);

                    $uniq = time();
                    $filename = "$dateFrom-$dateTo-$uniq.xls";
                    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    header("Content-Disposition: attachment;filename=\"{$filename}\"");
                    header("Cache-Control: max-age=0");

                    $user = Yii::$app->user->getIdentity();

                    $writer = new Xls($spreadsheet);
                    $writer->save("uploads/orders/{$user->id}/$filename");
                    $writer->save('php://output');
                    exit;
                } catch (Exception $e) {
                    Yii::error($e->getMessage());
                }
            }

            return $this->render('file_upload', ['model' => $meter]);
        }
    }
}
