<?php
namespace app\models\forms;

use app\models\db\User;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $xcode;
    public $email;
    public $password;

    public function rules()
    {
        return [
            ['xcode', 'trim'],
            ['xcode', 'required'],
            ['xcode', 'unique', 'targetClass' => '\app\models\db\User', 'message' => 'This username has already been taken.'],
            ['xcode', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\db\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'xcode' => 'X - код',
            'email' => 'Почта',
            'password' => 'Пароль',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws \Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->xcode = $this->xcode;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();


        $auth = \Yii::$app->authManager;
        $role = $auth->getRole(User::ROLE_USER);
        $auth->assign($role, $user->id);

        return $user->save() ? $user : null;
    }
}
