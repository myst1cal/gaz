<?php

use app\models\db\User;
use yii\db\Migration;

/**
 * Class m190126_172640_addZcodeTable
 */
class m190126_172640_addMeterTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%meter}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'provider_id' => $this->integer()->notNull(),
            'zcode' => $this->string()->notNull()->unique(),
            'title' => $this->string()->notNull(),
            'address' => $this->text()->notNull(),
            'deleted' => $this->smallInteger()->defaultValue(User::NOT_DELETED)
        ], $tableOptions);

        // creates index for column `author_id`
        $this->createIndex(
            'meter_user_id',
            'meter',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk_meter_user_id',
            'meter',
            'user_id',
            'user',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `author_id`
        $this->createIndex(
            'meter_provider_id',
            'meter',
            'provider_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk_meter_provider_id',
            'meter',
            'provider_id',
            'provider',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_meter_user_id', 'meter');
        $this->dropForeignKey('fk_meter_provider_id', 'meter');

        $this->dropIndex('meter_user_id', 'meter');
        $this->dropIndex('meter_provider_id', 'meter');

        $this->dropTable('{{%meter}}');
    }
}
