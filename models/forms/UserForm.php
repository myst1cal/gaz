<?php

namespace app\models\forms;

use app\models\db\User;
use yii\base\Model;

class UserForm extends Model
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $id;
    public $xcode;
    public $email;
    public $password;
    public $role;
    /** @var array $railways */
    public $railways;
    public $egrpo;
    public $title;
    public $position1_full_name;
    public $position2_full_name;
    public $signatory;
    public $contract_number;
    public $contract_conclusion_date;
    public $is_railway;

    public function attributeLabels()
    {
        return [
            'xcode' => 'x - код',
            'email' => 'Почта',
            'password' => 'Пароль',
            'role' => 'Роль',
            'railways' => 'Ж/Д',
            'egrpo' => 'ЕГРПО',
            'fileEncodePublicKey' => 'Открытый ключ подтверждения файла',
            'title' => 'Название организации',
            'position1_full_name' => 'ФИО Директора',
            'position2_full_name' => 'ФИО Бухгалтера',
            'signatory' => 'Подписант',
            'contract_number' => 'Номер договора',
            'contract_conclusion_date' => 'Дата заключения договора',
            'is_railway' => 'Пользователь ж/д',
        ];
    }

    public function rules()
    {
        return [
            ['id', 'integer'],

            ['xcode', 'trim'],
            ['xcode', 'required'],
            [
                'xcode', 'unique', 'targetClass' => '\app\models\db\User',
                'message' => 'This username has already been taken.', 'except' => self::SCENARIO_UPDATE
            ],
            ['xcode', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [
                'email', 'unique', 'targetClass' => '\app\models\db\User',
                'message' => 'This email address has already been taken.', 'except' => self::SCENARIO_UPDATE
            ],

            ['password', 'required', 'except' => self::SCENARIO_UPDATE],
            ['password', 'string', 'min' => 6],

            ['role', 'required'],
            ['role', 'string'],

            ['railways', 'default', 'value' => []],

            ['egrpo', 'integer', 'max' => 99999999],

            ['title', 'trim'],
            ['title', 'required'],
            ['title', 'string'],

            ['position1_full_name', 'trim'],
            ['position1_full_name', 'required'],
            ['position1_full_name', 'string'],

            ['position2_full_name', 'trim'],
            ['position2_full_name', 'string'],

            ['signatory', 'trim'],
            ['signatory', 'required'],
            ['signatory', 'integer'],

            ['contract_number', 'trim'],
            ['contract_number', 'string'],

            ['contract_conclusion_date', 'trim'],
            ['is_railway', 'integer'],
        ];
    }

    public function getIsRailwayList() {
        return [
            User::RAILWAY => 'Да',
            User::NOT_RAILWAY => 'Нет',
        ];
    }
}
