<?php


namespace app\dto\phone;


class PhoneDto
{
    public $id;
    public $userId;
    public $number;
    public $comment;
}