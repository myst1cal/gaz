<?php


namespace app\services;


use app\models\db\AuthAssignment;
use app\models\db\User;
use app\models\db\UserRailway;
use app\models\forms\UserForm;
use Yii;

class UserService
{
    public static function createFromUserForm(UserForm $form)
    {
        if ($form->id) {
            $user = User::findOne($form->id);
        } else {
            $user = new User();
        }

        $user->xcode = $form->xcode;
        $user->email = $form->email;
        $user->egrpo = $form->egrpo;
        $user->title = $form->title;
        $user->position1_full_name = $form->position1_full_name;
        $user->position2_full_name = $form->position2_full_name;
        $user->signatory = $form->signatory;
        $user->contract_number = $form->contract_number;
        $user->contract_conclusion_date = $form->contract_conclusion_date;
        $user->is_railway = $form->is_railway;
        if ($form->password) {
            $user->setPassword($form->password);
        }

        $user->generateAuthKey();

        if ($user->save()) {
            if ($form->id) {
                UserRailway::deleteAll(['user_id' => $form->id]);
            }

            foreach ($form->railways as $railway) {
                $userRailway = new UserRailway();
                $userRailway->railway_id = $railway;
                $userRailway->user_id = $user->id;

                $userRailway->save();
            }

            AuthAssignment::deleteAll(['user_id' => $user->id]);

            $auth = Yii::$app->authManager;
            $role = $auth->getRole($form->role);
            $auth->assign($role, $user->id);

            return $user->id;
        }

        return null;
    }

    /**
     * @param integer $id
     * @return UserForm
     */
    public static function createUserFormById($id)
    {
        $user = User::findOne($id);

        $form = new UserForm();
        $form->id = $user->id;
        $form->xcode = $user->xcode;
        $form->email = $user->email;
        $form->role = $user->getRole();
        $form->egrpo = $user->egrpo;
        $form->title = $user->title;
        $form->position1_full_name = $user->position1_full_name;
        $form->position2_full_name = $user->position2_full_name;
        $form->signatory = $user->signatory;
        $form->contract_number = $user->contract_number;
        $form->contract_conclusion_date = $user->contract_conclusion_date;
        $form->is_railway = $user->is_railway;

        foreach ($user->railways as $railway) {
            $form->railways[] = $railway->id;
        }

        return $form;
    }
}