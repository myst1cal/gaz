<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "order_history".
 *
 * @property int $id
 * @property int|null $file_id
 * @property int|null $meter_id
 * @property int|null $date_time Время сохранения
 * @property string|null $year
 * @property string|null $month
 * @property float|null $day_1
 * @property float|null $day_2
 * @property float|null $day_3
 * @property float|null $day_4
 * @property float|null $day_5
 * @property float|null $day_6
 * @property float|null $day_7
 * @property float|null $day_8
 * @property float|null $day_9
 * @property float|null $day_10
 * @property float|null $day_11
 * @property float|null $day_12
 * @property float|null $day_13
 * @property float|null $day_14
 * @property float|null $day_15
 * @property float|null $day_16
 * @property float|null $day_17
 * @property float|null $day_18
 * @property float|null $day_19
 * @property float|null $day_20
 * @property float|null $day_21
 * @property float|null $day_22
 * @property float|null $day_23
 * @property float|null $day_24
 * @property float|null $day_25
 * @property float|null $day_26
 * @property float|null $day_27
 * @property float|null $day_28
 * @property float|null $day_29
 * @property float|null $day_30
 * @property float|null $day_31
 */
class OrderHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_id', 'meter_id', 'date_time'], 'integer'],
            [['day_1', 'day_2', 'day_3', 'day_4', 'day_5', 'day_6', 'day_7', 'day_8', 'day_9', 'day_10', 'day_11', 'day_12', 'day_13', 'day_14', 'day_15', 'day_16', 'day_17', 'day_18', 'day_19', 'day_20', 'day_21', 'day_22', 'day_23', 'day_24', 'day_25', 'day_26', 'day_27', 'day_28', 'day_29', 'day_30', 'day_31'], 'number'],
            [['year'], 'string', 'max' => 4],
            [['month'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_id' => 'File ID',
            'meter_id' => 'Meter ID',
            'date_time' => 'Date Time',
            'year' => 'Year',
            'month' => 'Month',
            'day_1' => 'Day 1',
            'day_2' => 'Day 2',
            'day_3' => 'Day 3',
            'day_4' => 'Day 4',
            'day_5' => 'Day 5',
            'day_6' => 'Day 6',
            'day_7' => 'Day 7',
            'day_8' => 'Day 8',
            'day_9' => 'Day 9',
            'day_10' => 'Day 10',
            'day_11' => 'Day 11',
            'day_12' => 'Day 12',
            'day_13' => 'Day 13',
            'day_14' => 'Day 14',
            'day_15' => 'Day 15',
            'day_16' => 'Day 16',
            'day_17' => 'Day 17',
            'day_18' => 'Day 18',
            'day_19' => 'Day 19',
            'day_20' => 'Day 20',
            'day_21' => 'Day 21',
            'day_22' => 'Day 22',
            'day_23' => 'Day 23',
            'day_24' => 'Day 24',
            'day_25' => 'Day 25',
            'day_26' => 'Day 26',
            'day_27' => 'Day 27',
            'day_28' => 'Day 28',
            'day_29' => 'Day 29',
            'day_30' => 'Day 30',
            'day_31' => 'Day 31',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeter()
    {
        return $this->hasOne(Meter::className(), ['id' => 'meter_id']);
    }
}
