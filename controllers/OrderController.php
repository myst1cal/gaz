<?php

namespace app\controllers;

use app\dto\date\DateDto;
use app\dto\order\UserXlsDataRowDto;
use app\helpers\DateHelper;
use app\models\db\Meter;
use app\models\db\Order;
use app\models\db\User;
use app\models\forms\OrderForm;
use app\models\searches\OrderSearch;
use app\repositories\MeterRepository;
use app\repositories\OrderRepository;
use app\repositories\RailwayRepository;
use app\services\OrderService;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xls\Color\BIFF8;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use function GuzzleHttp\Psr7\str;


class OrderController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_USER],
                    ],
                    [
                        'allow' => ['print', 'all'],
                        'roles' => [User::ROLE_USER],
                    ],
                    [
                        'actions' => ['current-month', 'next-month', 'table'],
                        'allow' => true,
                        'roles' => [User::ROLE_USER, User::ROLE_ADMINISTRATOR],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->getId());

        $user = Yii::$app->user->getIdentity();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'meterList' => MeterRepository::getListIdZcode($user),
        ]);
    }

    public function actionView($id)
    {
        if (Yii::$app->user->can('orderAccess', ['orderId' => $id])) {
            $order = OrderRepository::getById($id);
            return $this->render('view', [
                'model' => $order,
            ]);
        }
    }

    public function actionCreate()
    {
        $form = new OrderForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            if ($id = OrderService::createFromOrderForm($form)) {
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        $user = Yii::$app->user->identity;
        return $this->render('create', [
            'model' => $form,
            'startDate' => OrderService::getStartDate(),
            'meterList' => MeterRepository::getListIdZcode($user),
            'railwayList' => RailwayRepository::getListIdTitle($user),
        ]);
    }

    public function actionUpdate($id)
    {
        if (Yii::$app->user->can('orderAccess', ['orderId' => $id])) {
            $model = OrderRepository::getById($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            $user = Yii::$app->user->identity;
            return $this->render('update', [
                'model' => $model,
                'startDate' => OrderService::getStartDate(),
                'meterList' => MeterRepository::getListIdZcode($user),
                'railwayList' => RailwayRepository::getListIdTitle($user),
            ]);
        }
    }

    public function actionRenominate($id)
    {
        $model = OrderRepository::getById($id);

        $model->scenario = Order::SCENARIO_RENOMINATE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('renominate', [
            'model' => $model,
        ]);
    }

    public function actionPrint()
    {
        $dateFrom = Yii::$app->request->post('date_from');
        $dateTo = Yii::$app->request->post('date_to');
        $meterId = Yii::$app->request->post('meter_id');

        if (strlen($dateFrom) == 7) {
            $dateFrom = $dateFrom . '-01';
            $daysInMonth = date('t', strtotime($dateTo . '-01'));
            $dateTo = $dateTo . '-' . $daysInMonth;
        }

        $data = OrderService::getUserXlsData($dateFrom, $dateTo, $meterId);

        $spreadsheet = new Spreadsheet();
        try {
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValue('A' . 1, 'Дата');
            $sheet->setCellValue('B' . 1, 'Номинация, тыс. м3');
            $sheet->setCellValue('C' . 1, 'Реноминация, тыс. м3');
            $sheet->setCellValue('D' . 1, 'z - код');

            $rowNumber = 2;
            if ($data->rows) {
                foreach ($data->rows as $rowDto) {
                    $sheet->setCellValue('A' . $rowNumber, $rowDto->date);
                    $sheet->setCellValue('B' . $rowNumber, $rowDto->nomination);
                    $sheet->setCellValue('C' . $rowNumber, $rowDto->renomination);
                    $sheet->setCellValue('D' . $rowNumber, $rowDto->zcode);

                    $rowNumber++;
                }
            }

            $filename = $dateFrom . '-' . $dateTo . '.xls';
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');

            $writer = new Xls($spreadsheet);
            $writer->save('php://output');
        } catch (Exception $e) {
            Yii::error($e->getMessage());
        }
    }

    public function actionCurrentMonth()
    {
        $meterId = Yii::$app->request->get('meter_id');
        /** @var User $user */
        $user = Yii::$app->user->getIdentity();
        $print = Yii::$app->request->post('print');

        if (!Meter::findOne(['id' => $meterId, 'user_id' => $user->id])) {
            throw new ForbiddenHttpException('NotFound', '403');
        }

        $date = DateHelper::getDates(DateHelper::CURRENT_MONTH);

        if (Yii::$app->request->isPost && !$print) {
            $nominations = Yii::$app->request->post('nomination', []);
            $renominations = Yii::$app->request->post('renomination', []);

            OrderService::saveNominations($nominations, $meterId, $date);
            OrderService::saveRenominations($renominations, $meterId, $date);

            Yii::$app->session->setFlash('success', 'Изменения сохранены.');
        }

        $this->layout = 'withoutBorder';

        $data = OrderService::getOrdersByDayAndSums($date, $meterId);

        if ($print) {
            $meter = Meter::findOne($meterId);
            $sum = Yii::$app->request->post('nomination') ? $data['sums']['nomination'] : $data['sums']['renomination'];

            $startDate = "1.$date->month.$date->year";
            $endDate = "{$data['daysInMonth']}.$date->month.$date->year";

            return $this->renderPartial('print-table',
                [
                    'dataByMonths' => [
                        [
                            'days' => range(1, $data['daysInMonth']),
                            'meter' => $meter,
                            'sum' => $sum,
                            'orderByDay' => $data['print']['days'],
                            'endDay' => $data['daysInMonth'],
                            'month' => $date->month,
                            'year' => $date->year,
                        ]
                    ],
                    'endDate' => $endDate,
                    'meter' => $meter,
                    'days' => range(1, $data['daysInMonth']),
                    'startDate' => $startDate
                ]
            );
        }

        return $this->render('table', [
            'date' => $date->month . '.' . $date->year,
            'days' => range(1, $data['daysInMonth']),
            'nominationSum' => $data['sums']['nomination'],
            'renominationSum' => $data['sums']['renomination'],
            'orders' => $data['days'],
        ]);
    }

    public function actionNextMonth()
    {
        $this->layout = 'withoutBorder';

        $meterId = Yii::$app->request->get('meter_id');
        $print = Yii::$app->request->post('print');

        /** @var User $user */
        $user = Yii::$app->user->getIdentity();

        if (!Meter::findOne(['id' => $meterId, 'user_id' => $user->id])) {
            throw new ForbiddenHttpException('NotFound', '403');
        }

        $date = DateHelper::getDates(DateHelper::NEXT_MONTH);

        if (Yii::$app->request->isPost && !$print) {
            $nominations = Yii::$app->request->post('nomination', []);
            $renominations = Yii::$app->request->post('renomination', []);

            OrderService::saveNominations($nominations, $meterId, $date);
            OrderService::saveRenominations($renominations, $meterId, $date);

            Yii::$app->session->setFlash('success', 'Изменения сохранены.');
        }

        $data = OrderService::getOrdersByDayAndSums($date, $meterId);

        if ($print) {
            $meter = Meter::findOne($meterId);
            $sum = Yii::$app->request->post('nomination') ? $data['sums']['nomination'] : $data['sums']['renomination'];

            $startDate = "1.$date->month.$date->year";
            $endDate = "{$data['daysInMonth']}.$date->month.$date->year";

            return $this->renderPartial('print-table',
                [
                    'dataByMonths' => [
                        [
                            'days' => range(1, $data['daysInMonth']),
                            'meter' => $meter,
                            'sum' => $sum,
                            'orderByDay' => $data['print']['days'],
                            'endDay' => $data['daysInMonth'],
                            'month' => $date->month,
                            'year' => $date->year,
                        ]
                    ],
                    'endDate' => $endDate,
                    'meter' => $meter,
                    'days' => range(1, $data['daysInMonth']),
                    'startDate' => $startDate
                ]
            );
        }

        return $this->render('table', [
            'date' => $date->month . '.' . $date->year,
            'days' => range(1, $data['daysInMonth']),
            'nominationSum' => $data['sums']['nomination'],
            'renominationSum' => $data['sums']['renomination'],
            'orders' => $data['days'],
        ]);
    }

    public function actionPrintTable()
    {
        $meterId = Yii::$app->request->post('meter_id');
        $from = Yii::$app->request->post('date_from');
        $to = Yii::$app->request->post('date_to');

        /** @var User $user */
        $user = Yii::$app->user->getIdentity();

        if (!Meter::findOne(['id' => $meterId, 'user_id' => $user->id])) {
            throw new ForbiddenHttpException('NotFound', '403');
        }

        $dates = [];
        list($yearFrom, $monthFrom) = explode('-', $from);
        list($yearTo, $monthTo) = explode('-', $to);

        for ($i = 0, $break = true, $currentYear = $yearFrom; $break; $currentYear++, $i++) {
            for ($currentMonth = $i ? 1 : $monthFrom; $currentMonth < 13 && $break; $currentMonth++) {
                if ($currentMonth == $monthTo && $currentYear == $yearTo) {
                    $break = false;
                }

                $date = new DateDto();
                $date->year = $currentYear;
                $date->month = $currentMonth;

                $dates[] = $date;
            }
        }

        $dataByMonths = [];
        $meter = Meter::findOne($meterId);

        $endDay = 1;
        foreach ($dates as $date) {

            $data = OrderService::getOrdersByDayAndSums($date, $meterId);
            $sum = $data['sums']['renomination'];

            $endDay = $data['daysInMonth'];

            $dataByMonths[$date->year . '-' . $date->month] = [
                'days' => range(1, $data['daysInMonth']),
                'meter' => $meter,
                'sum' => $sum,
                'orderByDay' => $data['print']['days'],
                'endDay' => $data['daysInMonth'],
                'month' => $date->month,
                'year' => $date->year,
            ];
        }

        $startDate = "1.$monthFrom.$yearFrom";
        $endDate = "$endDay.$monthTo.$yearTo";

        return $this->renderPartial('print-table',
            [
                'dataByMonths' => $dataByMonths,
                'meter' => $meter,
                'days' => range(1, 31),
                'endDate' => $endDate,
                'startDate' => $startDate,
            ]
        );
    }

    public function actionTable()
    {
        $this->layout = 'withoutBorder';

        $meterId = Yii::$app->request->get('meter_id');
        $print = Yii::$app->request->post('print');
        $printDate = Yii::$app->request->post('print_date');
        $from = Yii::$app->request->get('date_from');
        $to = Yii::$app->request->get('date_to');

        /** @var User $user */
        $user = Yii::$app->user->getIdentity();

        if (!Meter::findOne(['id' => $meterId, 'user_id' => $user->id])) {
            throw new ForbiddenHttpException('NotFound', '403');
        }

        $dates = [];
        list($yearFrom, $monthFrom) = explode('-', $from);
        list($yearTo, $monthTo) = explode('-', $to);

        for ($i = 0, $break = true, $currentYear = $yearFrom; $break; $currentYear++, $i++) {
            for ($currentMonth = $i ? 1 : $monthFrom; $currentMonth < 13 && $break; $currentMonth++) {
                if ($currentMonth == $monthTo && $currentYear == $yearTo) {
                    $break = false;
                }

                $date = new DateDto();
                $date->year = $currentYear;
                $date->month = $currentMonth;

                $dates[] = $date;
            }
        }

        if (Yii::$app->request->isPost && !$print) {
            $nominations = Yii::$app->request->post('nomination', []);
            $renominations = Yii::$app->request->post('renomination', []);

            foreach ($nominations as $yearMonth => $nominationMonthData) {
                $date = new DateDto();
                list($date->year, $date->month) = explode('-', $yearMonth);

                OrderService::saveNominations($nominationMonthData, $meterId, $date);
            }
            foreach ($renominations as $yearMonth => $renominationMonthData) {
                $date = new DateDto();
                list($date->year, $date->month) = explode('-', $yearMonth);

                OrderService::saveRenominations($renominationMonthData, $meterId, $date);
            }

            Yii::$app->session->setFlash('success', 'Изменения сохранены.');
        }

        foreach ($dates as $date) {

            $data = OrderService::getOrdersByDayAndSums($date, $meterId);
            if ($print && $printDate == $date->year . '-' . $date->month) {
                $meter = Meter::findOne($meterId);
                $sum = Yii::$app->request->post('nomination') ? $data['sums']['nomination'] : $data['sums']['renomination'];

                $startDate = "1.{$date->month}.{$date->year}";
                $endDate = "{$data['daysInMonth']}.{$date->month}.{$date->year}";

                return $this->renderPartial('print-table',
                    [
                        'dataByMonths' => [
                            [
                                'days' => range(1, $data['daysInMonth']),
                                'meter' => $meter,
                                'sum' => $sum,
                                'orderByDay' => $data['print']['days'],
                                'month' => $date->month,
                                'year' => $date->year,
                            ]
                        ],
                        'endDate' => $endDate,
                        'startDate' => $startDate,
                        'days' => range(1, $data['daysInMonth']),
                        'meter' => $meter,
                    ]
                );
            }

            $dataByMonth[$date->year . '-' . $date->month] = [
                'from' => $from,
                'to' => $to,
                'meterId' => $meterId,
                'date' => $date->month . '.' . $date->year,
                'days' => range(1, $data['daysInMonth']),
                'nominationSum' => $data['sums']['nomination'],
                'renominationSum' => $data['sums']['renomination'],
                'orders' => $data['days'],
            ];
        }
        return $this->render('tables', [
            'meterId' => $meterId, 'from' => $from, 'to' => $to, 'dataByMonth' => $dataByMonth
        ]);
    }

    public function actionAll()
    {
        $this->layout = 'withoutBorder';

        /** @var User $user */
        $user = Yii::$app->user->getIdentity();

        $dataForm = Yii::$app->request->post('data');

        $dateFrom = Yii::$app->request->post('date_from');
        $dateTo = Yii::$app->request->post('date_to');
        $toExcel = Yii::$app->request->post('to_excel');

        if ($dateFrom && $dateTo && $toExcel) {
            $dateFrom = date('Y-m') . '-01';
            $dateTo = date("Y-m-t", strtotime($dateTo . '-01'));

            $printData = [];
            $metersData = Meter::find()->where(['user_id' => $user->getId()])->all();

            list($yearFrom, $monthFrom) = explode('-', $dateFrom);
            list($yearTo, $monthTo) = explode('-', $dateTo);
            $dates = [];
            for ($i = 0, $break = true, $currentYear = $yearFrom; $break; $currentYear++, $i++) {
                for ($currentMonth = $i ? 1 : $monthFrom; $currentMonth < 13 && $break; $currentMonth++) {
                    if ($currentMonth == $monthTo && $currentYear == $yearTo) {
                        $break = false;
                    }

                    $date = new DateDto();
                    $date->year = $currentYear;
                    $date->month = $currentMonth;

                    $dates[] = "$currentYear-$currentMonth";
                }
            }

            /** @var Meter $meterItem */
            for ($i = 0; $i < count($dates); ++$i) {
                $leftDate = $dates[$i] . '-01';
                $rightDate = date("Y-m-t", strtotime($dates[$i] . '-01'));
                $dateKey = date('m.Y', strtotime($dates[$i] . '-01'));

                foreach ($metersData as $meterItem) {
                    $key = $meterItem->provider_id;

                    $orders = Order::find()
                        ->where(['>=', 'date', $leftDate])
                        ->andWhere(['<=', 'date', $rightDate])
                        ->andWhere(['meter_id' => $meterItem->id])
                        ->all();

                    $orders = ArrayHelper::index($orders, function ($i) {
                        return (int)(new \DateTime($i->date))->format('d');
                    });

                    $printData[$dateKey][$key]['provider_name'] = $meterItem->provider->title;
                    $printData[$dateKey][$key]['meters'][] = [
                        'zcode' => $meterItem->zcode,
                        'address' => $meterItem->address,
                        'title' => $meterItem->title,
                        'xcode' => $meterItem->user->xcode,
                        'orders' => $orders
                    ];

                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                    $spreadsheet = $reader->load("upload_file_templates/application_order_5.xlsx");

                    try {
                        $sheet = $spreadsheet->getActiveSheet();

                        $totalPerMonthByProvider = 0;
                        $totalByDay = [];

                        $row = 11;
                        foreach ($printData as $dataKey => $providersData) {
                            $sheet->setCellValueByColumnAndRow(1, $row, $dataKey);
                            $spreadsheet->getActiveSheet()
                                ->getStyle("A$row:AJ$row")
                                ->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()
                                ->setRGB('b9cde5');
                            $spreadsheet->getActiveSheet()
                                ->getStyle("A$row:AJ$row")
                                ->getFont()
                                ->setBold(1);
                            ++$row;

                            foreach ($providersData as $providerId => $providerData) {
                                $perMonthByProvider = 0;
                                $sheet->setCellValueByColumnAndRow(1, $row, $providerData['provider_name']);

                                $spreadsheet->getActiveSheet()
                                    ->getStyle("A$row:AJ$row")
                                    ->getFill()
                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                    ->getStartColor()
                                    ->setRGB('b9cde5');
                                $spreadsheet->getActiveSheet()
                                    ->getStyle("A$row:AJ$row")
                                    ->getFont()
                                    ->setBold(1);

                                ++$row;
                                $sheet->insertNewRowBefore($row);

                                $byDay = [];
                                foreach ($providerData['meters'] as $meterValue) {
                                    $perMonth = 0;
                                    foreach (range(1, 31) as $day) {
                                        $currentDayValue = $meterValue['orders'][$day] ?? 0;

                                        if ($currentDayValue) {
                                            $value = $currentDayValue->renomination > 0 ? $currentDayValue->renomination : $currentDayValue->nomination;
                                            $perMonth += $value;
                                            $byDay[$day] = $byDay[$day] ?? 0 + $value;
                                            $sheet->setCellValueByColumnAndRow(5 + $day, $row, $value);
                                        }
                                    }
                                    $perMonthByProvider += $perMonth;

                                    $sheet->setCellValueByColumnAndRow(1, $row, $meterValue['title']);
                                    $sheet->setCellValueByColumnAndRow(2, $row, $meterValue['address']);
                                    $sheet->setCellValueByColumnAndRow(3, $row, $perMonth);
                                    $sheet->setCellValueByColumnAndRow(4, $row, $meterValue['xcode']);
                                    $sheet->setCellValueByColumnAndRow(5, $row, $meterValue['zcode']);

                                    $spreadsheet->getActiveSheet()
                                        ->getStyle("A$row:AJ$row")
                                        ->getFill()
                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()
                                        ->setRGB('ffffff');

                                    $spreadsheet->getActiveSheet()
                                        ->getStyle("A$row:AJ$row")
                                        ->getFont()
                                        ->setBold(0);

                                    ++$row;
                                    $sheet->insertNewRowBefore($row);
                                }


                                $sheet->setCellValueByColumnAndRow(1, $row, 'Разом по ' . $providerData['provider_name']);
                                $sheet->setCellValueByColumnAndRow(3, $row, $perMonthByProvider);

                                $totalPerMonthByProvider += $perMonthByProvider;

                                foreach (range(1, 31) as $day) {
                                    $totalByDay[$day] = $byDay[$day] ?? 0;
                                    $sheet->setCellValueByColumnAndRow(5 + $day, $row, $byDay[$day] ?? 0);
                                }

                                $spreadsheet->getActiveSheet()
                                    ->getStyle("A$row:AJ$row")
                                    ->getFill()
                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                    ->getStartColor()
                                    ->setRGB('b9cde5');
                                $spreadsheet->getActiveSheet()
                                    ->getStyle("A$row:AJ$row")
                                    ->getFont()
                                    ->setBold(1);

                                ++$row;
                                $sheet->insertNewRowBefore($row);
                            }
                        }

                        $sheet->setCellValueByColumnAndRow(1, $row, 'Загальна потреба , тис. м куб.');
                        $sheet->setCellValueByColumnAndRow(3, $row, $totalPerMonthByProvider);
                        $sheet->setCellValueByColumnAndRow(3, 10, $totalPerMonthByProvider);
                        foreach (range(1, 31) as $day) {
                            $sheet->setCellValueByColumnAndRow(5 + $day, $row, $totalByDay[$day] ?? 0);
                        }

                        $spreadsheet->getActiveSheet()
                            ->getStyle("A$row:AJ$row")
                            ->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()
                            ->setRGB('b9cde5');
                        $spreadsheet->getActiveSheet()
                            ->getStyle("A$row:AJ$row")
                            ->getFont()
                            ->setBold(1);

                        $row += 3;
                        $sheet->setCellValueByColumnAndRow(2, $row, $user->signatoryName());
                        $sheet->setCellValueByColumnAndRow(5, $row, $user->signatoryUserName());

                        $uniq = time();
                        $filename = "$dateFrom-$dateTo-$uniq.xls";
                        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                        header("Content-Disposition: attachment;filename=\"{$filename}\"");
                        header("Cache-Control: max-age=0");

                        $writer = new Xls($spreadsheet);

                        if (!file_exists("uploads/orders/{$user->id}/")) {
                            mkdir("uploads/orders/{$user->id}/", 0777, true);
                        }

                        $writer->save("uploads/orders/{$user->id}/$filename");
                        $writer->save('php://output');
                        exit;
                    } catch (Exception $e) {
                        Yii::error($e->getMessage());
                    }
                }
            }
        }

        $errors = [];
        if (Yii::$app->request->isPost && $dataForm) {
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();

            try {
                foreach ($dataForm as $zcode => $dates) {
                    foreach ($dates as $date => $days) {
                        foreach ($days as $day => $values) {
                            if (isset($values['nomination']) && $values['nomination'] == 0) {
                                continue;
                            }

                            $fullDate = $date . '-' . $day;
                            $order = Order::find()
                                ->leftJoin(Meter::tableName(), 'meter.id=order.meter_id')
                                ->where(['order.date' => $fullDate])
                                ->andWhere(['meter.zcode' => $zcode, 'meter.user_id' => $user->id])
                                ->one();

                            if (!$order) {
                                $meter = Meter::findOne(['zcode' => $zcode]);

                                $order = new Order();
                                $order->date = $fullDate;
                                $order->meter_id = $meter->id;

                            }

                            if (isset($values['nomination']) && $order->nomination != $values['nomination']) {
                                if (OrderService::canUpdateInTable($fullDate)) {
                                    $order->nomination = $values['nomination'];

                                    $order->save();
                                } else {
                                    $errors[] = "$zcode: $fullDate - нельзя провести номинацию";
                                }
                            }

                            if (isset($values['renomination']) && $order->renomination != $values['renomination']) {
                                if (OrderService::canRenominateInTable($fullDate)) {
                                    $order->renomination = $values['renomination'];

                                    $order->save();
                                } else {
                                    $errors[] = "$zcode: $fullDate - нельзя провести реноминацию";
                                }
                            }
                        }
                    }
                }

                if (empty($errors)) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            } catch (\Throwable $throwable) {
                $transaction->rollBack();
            }
        }

        if (empty($dateFrom)) {
            $dateFrom = date('Y-m');
        }

        if (empty($dateTo)) {
            $dateTo = date('Y-m');
        }

        list($yearFrom, $monthFrom) = explode('-', $dateFrom);
        list($yearTo, $monthTo) = explode('-', $dateTo);

        $dates = [];
        for ($i = 0, $break = true, $currentYear = $yearFrom; $break; $currentYear++, $i++) {
            for ($currentMonth = $i ? 1 : $monthFrom; $currentMonth < 13 && $break; $currentMonth++) {
                if ($currentMonth == $monthTo && $currentYear == $yearTo) {
                    $break = false;
                }

                $date = new DateDto();
                $date->year = $currentYear;
                $date->month = $currentMonth;

                $dates[] = $date;
            }
        }

        $meters = Meter::findAll(['user_id' => $user->getId()]);
        foreach ($meters as $meter) {
            foreach ($dates as $date) {
                $data = OrderService::getOrdersByDayAndSums($date, $meter->id);

                foreach ($data['days'] as $day => $temp) {
                    $oldValueNomination = $dataForm[$meter->zcode][$date->year . '-' . $date->month][$day]['nomination'] ?? 0;
                    $oldValueRenomination = $dataForm[$meter->zcode][$date->year . '-' . $date->month][$day]['renomination'] ?? 0;

                    if ($oldValueNomination || $oldValueRenomination) {

                        $data['days'][$day] = new Order([
                            'date' => $date->year . '-' . $date->month . '-' . $day,
                            'nomination' => $oldValueNomination, 'renomination' => $oldValueRenomination
                        ]);
                    }
                }

                $dataByZcodeAndMonth[$meter->zcode][$date->year . '-' . $date->month] = [
                    'meterId' => $meter->id,
                    'date' => $date->month . '.' . $date->year,
                    'days' => range(1, $data['daysInMonth']),
                    'nominationSum' => $data['sums']['nomination'],
                    'renominationSum' => $data['sums']['renomination'],
                    'orders' => $data['days'],
                ];
            }
        }

        return $this->render('tables-all', ['dataByZcodeAndMonth' => $dataByZcodeAndMonth, 'errors' => $errors]);
    }
}
