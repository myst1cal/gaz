<?php

namespace app\dto\order;


class OrderFileDto
{
    /** @var string $url */
    public $url;

    /** @var string $fileName */
    public $fileName;

    /** @var string $fileNameEncode */
    public $fileNameEncode;

    /** @var integer $userId */
    public $userId;

}