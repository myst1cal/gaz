<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\searches\ProviderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Оператор ГТС/ГРМ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provider-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить поставщика', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'title',
//            'deleted',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {order_list}',
                'buttons' => [
                    'order_list' => function ($url, $model, $key) {
                        /** @var \app\models\db\Provider $model */
                        return Html::a('<span class="glyphicon glyphicon-list"></span>', ['order/list-orders-by-provider', 'providerId' => $model->id], ['title' => 'Список заказов']);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
