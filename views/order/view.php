<?php

use app\services\OrderService;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\db\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="order-view">

    <h1>Заказ № <?= Html::encode($model->id) ?></h1>

    <?= date('y-m-d H:i') ?>
    <p>
        <?php if (OrderService::canUpdate($model)): ?>
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>

        <?= Html::a('К списку', ['index', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Счетчик',
                'value' => function($model){
                    /** @var \app\models\db\Order $model */
                    return $model->meter->title ;
                }
            ],
            'nomination',
            'renomination',
            [
                'label' => 'Ж/д',
                'value' => function($model){
                    /** @var \app\models\db\Order $model */
                    return $model->railway->title ?: '' ;
                }
            ],
            'date:date',
        ],
    ]) ?>

</div>
