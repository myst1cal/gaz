<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\searches\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=>function ($model, $key, $index, $grid){
            $class = empty($model->file_id) ? 'back-red' : '';
            return [
                'class'=>$class
            ];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'X - код',
                'attribute' => 'xcode',
            ],
            [
                'label' => 'Z - код',
                'attribute' => 'zcode',
            ],
            'nomination',
            'renomination',
            'date',


            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{file}',
                'buttons' => [
                    'file' => function ($url, $model, $key) {
                        /** @var \app\models\db\Order $model */
                        $html = '';
                        if (!empty($model->file_id)) {
                            $html = Html::a('', '/' . $model->file->url . $model->file->file_name, ['title' => 'Файл', 'download' => true, 'class' => 'glyphicon glyphicon-download']);
                            $html .= Html::a('', '/' . $model->file->url . $model->file->file_name_encode, ['title' => 'Подписанный файл', 'download' => true, 'class' => 'glyphicon glyphicon-download']);
                        }
                        return $html;
                    }
                ]
            ],
        ],
    ]); ?>
</div>
