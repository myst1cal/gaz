<?php

namespace app\modules\admin\models\searches;

use app\models\db\Meter;
use app\models\db\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\db\Order;

/**
 * OrderSearch represents the model behind the search form of `app\models\db\Order`.
 */
class OrderSearch extends Order
{
    public $xcode, $zcode;
    public function rules()
    {
        return [
            [['id', 'meter_id', 'deleted'], 'integer'],
            [['nomination', 'renomination'], 'number'],
            [['xcode', 'zcode'], 'string'],
            [['date', ], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['date' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'meter_id' => $this->meter_id,
            'nomination' => $this->nomination,
            'renomination' => $this->renomination,
            'date' => $this->date,
            'deleted' => $this->deleted,
        ]);

        $query->leftJoin(Meter::tableName(), 'meter.id=order.meter_id');
        $query->leftJoin(User::tableName(), 'user.id=meter.user_id');
        $query->andFilterWhere(['like', 'meter.zcode', $this->zcode])
            ->andFilterWhere(['like', 'user.xcode', $this->xcode]);

        return $dataProvider;
    }
}
