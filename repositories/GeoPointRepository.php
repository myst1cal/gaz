<?php


namespace app\repositories;


use app\dto\geo\GeoPointDto;
use app\models\db\City;
use app\modules\weather\interfaces\IGeoPointsRepository;

class GeoPointRepository implements IGeoPointsRepository
{
    /**
     * @return GeoPointDto[]
     */
    public function getAllPoints(): array
    {
        $cities = City::find()->all();

        $response = [];

        foreach ($cities as $city) {
            $geoPoint = new GeoPointDto($city);

            $response[] = $geoPoint;
        }

        return $response;
    }
}