<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Meter */

$this->title = 'Редактирование счетчика: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="meter-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cityRepository' => $cityRepository,
    ]) ?>

</div>
