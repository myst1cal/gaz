<?php

use yii\db\Migration;

/**
 * Class m190314_174344_alterEgrpoColumnInUserTable
 */
class m190314_174344_alterEgrpoColumnInUserTable extends Migration
{
    public function safeUp()
    {
        $this->alterColumn(\app\models\db\User::tableName(), 'egrpo', $this->string(8)->null());
    }

    public function safeDown()
    {
        $this->alterColumn(\app\models\db\User::tableName(), 'egrpo', $this->integer()->null()->unsigned());
    }
}
