<?php

namespace app\models\searches;

use app\models\db\Meter;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\db\Order;


class OrderSearch extends Order
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'meter_id', 'deleted'], 'integer'],
            [['nomination', 'renomination'], 'number'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $userId = null)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'meter_id' => $this->meter_id,
            'date' => $this->date,
            'nomination' => $this->nomination,
            'renomination' => $this->renomination,
            'deleted' => $this->deleted,
        ]);

        if ($userId) {
            $query->leftJoin(Meter::tableName(), 'meter_id=meter.id')
                ->andFilterWhere(['meter.user_id' => $userId]);
        }

        return $dataProvider;
    }
}
