<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\db\Meter */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="meter-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('К списку', ['index', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Пользователь (x - код)',
                'value' => function($model){
                    /** @var \app\models\db\Meter $model */
                    return $model->user->xcode ;
                }
            ],
            [
                'label' => 'Город',
                'value' => function($model){
                    /** @var \app\models\db\Meter $model */
                    return $model->city->name ;
                }
            ],
            [
                'label' => 'Поставщик',
                'value' => function($model){
                    /** @var \app\models\db\Meter $model */
                    return $model->provider->title ;
                }
            ],
            'zcode',
            'title',
            'address:ntext',
        ],
    ]) ?>

</div>
