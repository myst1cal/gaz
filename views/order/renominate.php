<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\Order */

$this->title = 'Реноминация заказа: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Реноминация';
?>
<div class="order-renominate">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="order-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'meter_id')->dropDownList(\app\repositories\MeterRepository::getListIdZcode(Yii::$app->user->identity), ['disabled' => true]) ?>

        <?= $form->field($model, 'nomination')->input('numeric', ['disabled' => true]) ?>

        <?= $form->field($model, 'renomination')->input('numeric') ?>

        <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::className(),
            [
                'disabled' => true,
                'language' => 'ru',
                'removeButton' => false,
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]
        ) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
