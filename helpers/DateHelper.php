<?php

namespace app\helpers;


use app\dto\date\DateDto;

class DateHelper
{
    const NEXT_MONTH = false;
    const CURRENT_MONTH = true;

    public static function getDates($currentMonth = self::CURRENT_MONTH)
    {
        $date = new DateDto();

        if ($currentMonth) {
            $year = date('Y');
            $month = (string)date('m');
            $day = date('d');
        } else {
            $year = date('Y');
            $month = (string)date('m');
            if ($month == 12) {
                $month = '01';
                $year += 1;
            } else {
                $month += 1;
            }
            $day = date('d');
        }

        $date->year = $year;
        $date->month = $month;
        $date->day = $day;

        return $date;
    }

    public static function getFeatureMonths()
    {
        $yearCurrent = date('Y');
        $monthCurrent = date('m');

        $response = [];
        for ($year = $yearCurrent, $i = 0; $i < 2; $i++, $year++) {
            for ($month = 1; $month < 13; $month++) {
                $key = date('Y-m', strtotime($year . '-' . $month . '-1'));

                if ($year . (strlen($month) == 1 ? '0' . $month : $month) >= $yearCurrent . $monthCurrent) {
                    $response[$key] = $month . '.' . $year;
                }
            }
        }

        return $response;
    }

    public static function getUkrainianMonth(int $month)
    {
        $months = [1 => 'Січень', 2 => 'Лютий', 3 => 'Березень', 4 => 'Квітень', 5 => 'Травень', 6 => 'Червень', 7 => 'Липень', 8 => 'Серпень', 9 => 'Вересень', 10 => 'Жовтень', 11 => 'Листопад', 12 => 'Грудень'];

        return $months[$month];
    }

    public static function getMonthByUkrainian(string $monthName)
    {
        $months = ['Січень' => '01', 'Лютий' => '02', 'Березень' => '03', 'Квітень' => '04', 'Травень' => '05', 'Червень' => '06', 'Липень' => '07', 'Серпень' => '08', 'Вересень' => '09', 'Жовтень' => '10', 'Листопад' => '11', 'Грудень' => '12'];

        return $months[$monthName];
    }
}