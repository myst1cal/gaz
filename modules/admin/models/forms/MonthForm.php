<?php

namespace app\modules\admin\models\forms;


use yii\base\Model;

class MonthForm extends Model
{
    /** @var string $month */
    public $date;

    public $toXls;


    public function rules()
    {
        return [
            [['date'], 'string'],
            [['toXls'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'date' => 'Дата'
        ];
    }
}