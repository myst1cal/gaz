<?php

namespace app\repositories;


use app\models\db\Railway;
use app\models\db\User;
use app\models\db\UserRailway;
use yii\helpers\ArrayHelper;

class RailwayRepository
{
    public static function getListIdTitle(User $user = null)
    {
        $query = Railway::find();

        if ($user) {
            $query = $query
                ->leftJoin(UserRailway::tableName(), 'railway.id=user_railway.railway_id')
                ->where(['user_railway.user_id' => $user->getId()]);
        }

        $data = $query->all();

        return ArrayHelper::map($data, 'id', 'title');
    }
}