<?php


namespace app\services;


use app\dto\order\OrderFileDto;
use app\models\db\User;
use app\models\forms\OrderFileUploadForm;
use PhpOffice\PhpSpreadsheet\Calculation\Exception;

class FileUploaderService
{
    /**
     * @param OrderFileUploadForm $form
     * @param User $user
     * @return OrderFileDto
     * @throws Exception
     */
    public static function uploadOrderDocument(OrderFileUploadForm $form, User $user)
    {
        $dto = new OrderFileDto();
        $dto->url = 'uploads/orders/' . $user->id . '/';
        $dto->fileName = $form->orderFileEncode->baseName;
        $dto->fileNameEncode = $form->orderFileEncode->baseName . '.' . $form->orderFileEncode->extension;
        $dto->userId = $user->id;

        if (!file_exists($dto->url)) {
            mkdir($dto->url, 0777, true);
        }

        if (file_exists($dto->url . $dto->fileName)) {
            if ($form->validate() && !file_exists($dto->url . $dto->fileNameEncode) && $form->orderFileEncode->saveAs($dto->url . $dto->fileNameEncode)) {
                $oldFileData = file_get_contents($dto->url . $dto->fileName);
                $newFileData = file_get_contents($dto->url . $dto->fileNameEncode);

                // todo нет нормальной проверки на изменения файла
                if (mb_strpos($oldFileData, $newFileData) !== false || true) {
                    return $dto;
                }

                throw new Exception('Файл изменен');
            }

            throw new Exception('Не удалось сохранить файл');
        }

        throw new Exception('Файл не найден');
    }
}