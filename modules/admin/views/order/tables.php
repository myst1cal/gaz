<?php

use app\helpers\DateHelper;
use app\modules\admin\assets\TableAsset;
use app\services\OrderService;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataByMonth array */
/* @var $meterId integer */
/* @var $from string */
/* @var $to string */

$this->title = 'Заказ по месяцам';
$this->params['breadcrumbs'][] = $this->title;

TableAsset::register($this);
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::beginForm('/admin/order/print', 'post', ['class' => 'table-form']) ?>
    <?= Html::hiddenInput('meter_id', $meterId) ?>
    <div class="row">
        <div class="col-sm-2">
            <?= Html::dropDownList('date_from', $from, DateHelper::getFeatureMonths(), ['class' => 'form-control date-from']) ?>
        </div>
        <div class="col-sm-2">
            <?= Html::dropDownList('date_to', $to, DateHelper::getFeatureMonths(), ['class' => 'form-control date-to']) ?>
        </div>
        <div class="col-sm-1">
            <?= Html::submitButton('Применить', ['class' => 'btn btn-success table-submit']) ?>
        </div>
        <div class="col-sm-1">
            <?= Html::button('В excel', ['class' => 'btn btn-info table-to-excel']) ?>
        </div>
        <div class="col-sm-1">
            <?= Html::button('Печать', ['class' => 'btn btn-primary table-print']) ?>
        </div>
    </div>
    <?= Html::endForm() ?>

    <br>
    <?= Html::beginForm('', 'post') ?>
    <?php foreach ($dataByMonth as $date => $table): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= $table['date'] ?></h3>
            </div>
            <div class="panel-body">
                <p>
                    <?= Html::a('Печать номинации',
                        Url::current([], true), [
                            'target'      => '_blank',
                            'class'       => 'btn btn-info',
                            'data-method' => 'POST',
                            'data-params' => [
                                'print'      => true,
                                'nomination' => true,
                                'print_date' => $date,
                                'csrf_param' => Yii::$app->request->csrfParam,
                                'csrf_token' => Yii::$app->request->csrfToken,
                            ],
                        ]) ?>​
                    <?= Html::a('Печать реноминации',
                        Url::current([], true), [
                            'target'      => '_blank',
                            'class'       => 'btn btn-primary',
                            'data-method' => 'POST',
                            'data-params' => [
                                'print'        => true,
                                'renomination' => true,
                                'print_date'   => $date,
                                'csrf_param'   => Yii::$app->request->csrfParam,
                                'csrf_token'   => Yii::$app->request->csrfToken,
                            ],
                        ]) ?>​
                </p>

                <h3>Номинация</h3>

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Месяц</th>
                        <th>Сумма</th>
                        <?php foreach ($table['days'] as $day): ?>
                            <th><?= $day ?></th>
                        <?php endforeach; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?= $table['date'] ?></td>
                        <td ><?= $table['nominationSum'] ?></td>
                        <?php foreach ($table['days'] as $day): ?>
                            <td class="<?= date('w', strtotime($date . '-' . $day)) % 6 == 0 ? 'back-red' : '' ?>"><?= Html::textInput('nomination[' . $date . '][' . $day . ']', isset($table['orders'][$day]) ? $table['orders'][$day]->nomination : 0, ['disabled' => !OrderService::canUpdateInTable($table['orders'][$day]), 'class' => 'width40']) ?></td>
                        <?php endforeach; ?>
                    </tr>
                    </tbody>
                </table>

                <h3>Реноминация</h3>

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Месяц</th>
                        <th>Сумма</th>
                        <?php foreach ($table['days'] as $day): ?>
                            <th><?= $day ?></th>
                        <?php endforeach; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?= $table['date'] ?></td>
                        <td><?= $table['renominationSum'] ?></td>
                        <?php foreach ($table['days'] as $day): ?>
                            <td class="<?= date('w', strtotime($date . '-' . $day)) % 6 == 0 ? 'back-red' : '' ?>"><?= Html::textInput('renomination[' . $date . '][' . $day . ']', empty((double)$table['orders'][$day]->renomination) ? $table['orders'][$day]->nomination : $table['orders'][$day]->renomination, ['class' => 'width40', 'disabled' => !OrderService::canRenominateInTable($table['orders'][$day])]) ?></td>
                        <?php endforeach; ?>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endforeach; ?>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    <?= Html::endForm() ?>
</div>
