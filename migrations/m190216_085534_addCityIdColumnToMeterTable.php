<?php

use yii\db\Migration;

class m190216_085534_addCityIdColumnToMeterTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn(\app\models\db\Meter::tableName(), 'city_id', $this->integer()->null()->after('provider_id'));

        $this->createIndex(
            'meter_city_id',
            \app\models\db\Meter::tableName(),
            'city_id'
        );

        $this->addForeignKey(
            'fk_meter_city_id',
            \app\models\db\Meter::tableName(),
            'city_id',
            'city',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_meter_city_id', \app\models\db\Meter::tableName());

        $this->dropIndex('meter_city_id', \app\models\db\Meter::tableName());

        $this->dropColumn(\app\models\db\Meter::tableName(), 'city_id');
    }
}
