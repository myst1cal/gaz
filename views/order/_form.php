<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $startDate string */

/* @var $meterList array */
/* @var $railwayList array */

/* @var $categories array */
/* @var $minTemperaturePoints array */
/* @var $maxTemperaturePoints array */

?>
<div class="order-form">
    <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-lg-6">
            <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'meter')->dropDownList($meterList, ['class' => 'form-control meter', 'data-url' => \yii\helpers\Url::to('/forecast/chart-by-meter-id')]) ?>

            <?php if (!empty($railwayList)): ?>
                <?= $form->field($model, 'railway')->dropDownList($railwayList) ?>
            <?php endif; ?>

            <?= $form->field($model, 'nomination')->input('numeric') ?>

            <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::className(),
                [
                    'language' => 'ru',
                    'removeButton' => false,
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'startDate' => $startDate,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]
            ) ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

        </div>
        <div class="col-lg-6">
            <div id="weather-forecast-chart" class="weather-forecast-chart"></div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
\app\assets\OrderAsset::register($this);
?>
