<?php

namespace app\repositories;


use app\models\db\Meter;
use app\models\db\User;
use yii\helpers\ArrayHelper;

class MeterRepository
{
    public static function getListIdZcode(User $user = null)
    {
        $query = Meter::find();

        if ($user) {
            $query = $query->where(['user_id' => $user->getId()]);
        }

        $data = $query->all();

        return ArrayHelper::map($data, 'id', 'zcode');
    }

    /**
     * @param integer $id
     * @return Meter
     */
    public static function getById(int $id)
    {
        if (($model = Meter::findOne($id)) !== null) {
            return $model;
        }

        throw new \ExceptionException('Запрашиваемый счетчик не найден.');
    }
}