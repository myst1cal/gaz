<?php

namespace app\models\db;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "railway".
 *
 * @property int $id
 * @property string $title
 */
class Railway extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'railway';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
        ];
    }
}
