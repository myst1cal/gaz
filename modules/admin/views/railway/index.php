<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\searches\RailwaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ж/Д';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="railway-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить Ж/Д', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {order_list}',
                'buttons' => [
                    'order_list' => function ($url, $model, $key) {
                        /** @var \app\models\db\Railway $model */
                        return Html::a('<span class="glyphicon glyphicon-list"></span>', ['order/list-orders-by-railway', 'railwayId' => $model->id], ['title' => 'Список заказов']);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
