<?php

use app\assets\OrderAllAsset;
use app\assets\TableAsset;
use app\helpers\DateHelper;
use app\services\OrderService;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataByMonth array */
/* @var $meterId integer */
/* @var $from string */
/* @var $to string */

$this->title = 'Заказ по месяцам';
$this->params['breadcrumbs'][] = $this->title;

TableAsset::register($this);
OrderAllAsset::register($this);
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::beginForm('', 'post', ['class' => 'form-all']) ?>

        <?= Html::hiddenInput('to_excel', 1, ['class' => 'to-excel']) ?>
    <div class="row">
        <div class="col-lg-2">
            <?= Html::dropDownList('date_from', null, DateHelper::getFeatureMonths(), ['class' => 'form-control']) ?>
        </div>
        <div class="col-lg-2">
            <?= Html::dropDownList('date_to', null, DateHelper::getFeatureMonths(), ['class' => 'form-control']) ?>
        </div>
        <div class="col-lg-1">
            <?= Html::button('В excel', ['class' => 'btn btn-success submit-excel']) ?>
        </div>
        <div class="col-lg-1">
            <?= Html::button('Применить', ['class' => 'btn btn-info submit-supply']) ?>
        </div>
        <div class="col-lg-1">
            <?= Html::a('Загрузки', '/file/index', ['class' => 'btn btn-primary to-upload hide']) ?>
        </div>
    </div>
    <?= Html::endForm() ?>
    </p>

    <?php foreach ($errors as $message): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?= $message ?>
        </div>
    <?php endforeach; ?>

    <?= Html::beginForm('', 'post') ?>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <?php foreach ($dataByZcodeAndMonth as $zcode => $dataByMonth): ?>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading<?= $zcode ?>">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $zcode ?>"
                           aria-expanded="true" aria-controls="collapse<?= $zcode ?>">
                            Z-код: <?= $zcode ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse<?= $zcode ?>" class="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="heading<?= $zcode ?>">
                    <div class="panel-body">

                        <?php foreach ($dataByMonth as $date => $table): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?= $table['date'] ?></h3>
                                </div>
                                <div class="panel-body">
                                    <!--<p>
                                        <?= Html::a('Печать номинации',
                                        Url::current([], true), [
                                            'target' => '_blank',
                                            'class' => 'btn btn-info',
                                            'data-method' => 'POST',
                                            'data-params' => [
                                                'print' => true,
                                                'nomination' => true,
                                                'print_date' => $date,
                                                'csrf_param' => Yii::$app->request->csrfParam,
                                                'csrf_token' => Yii::$app->request->csrfToken,
                                            ],
                                        ]) ?>​
                                        <?= Html::a('Печать реноминации',
                                        Url::current([], true), [
                                            'target' => '_blank',
                                            'class' => 'btn btn-primary',
                                            'data-method' => 'POST',
                                            'data-params' => [
                                                'print' => true,
                                                'renomination' => true,
                                                'print_date' => $date,
                                                'csrf_param' => Yii::$app->request->csrfParam,
                                                'csrf_token' => Yii::$app->request->csrfToken,
                                            ],
                                        ]) ?>​
                                    </p>-->

                                    <h3>Номинация</h3>

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Месяц</th>
                                            <th>Сумма</th>
                                            <?php foreach ($table['days'] as $day): ?>
                                                <th><?= $day ?></th>
                                            <?php endforeach; ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?= $table['date'] ?></td>
                                            <td><?= $table['nominationSum'] ?></td>
                                            <?php foreach ($table['days'] as $day): ?>
                                                <td class="<?= date('w', strtotime($date . '-' . $day)) % 6 == 0 ? 'back-red' : '' ?>"><?= Html::textInput('data[' . $zcode . '][' . $date . '][' . $day . '][nomination]', $table['orders'][$day]->nomination ?? 0, [
                                                        'disabled' => !OrderService::canUpdateInTable($table['orders'][$day]),
                                                        'class' => 'width40'
                                                    ]) ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <h3>Реноминация</h3>

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Месяц</th>
                                            <th>Сумма</th>
                                            <?php foreach ($table['days'] as $day): ?>
                                                <th><?= $day ?></th>
                                            <?php endforeach; ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?= $table['date'] ?></td>
                                            <td><?= $table['renominationSum'] ?></td>
                                            <?php foreach ($table['days'] as $day): ?>
                                                <td class="<?= date('w', strtotime($date . '-' . $day)) % 6 == 0 ? 'back-red' : '' ?>"><?= Html::textInput('data[' . $zcode . '][' . $date . '][' . $day . '][renomination]', (double)($table['orders'][$day]->renomination ?? $table['orders'][$day]->nomination ?? 0), [
                                                        'class' => 'width40',
                                                        'disabled' => !OrderService::canRenominateInTable($table['orders'][$day])
                                                    ]) ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    <?= Html::endForm() ?>
</div>
