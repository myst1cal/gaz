<?php

namespace app\modules\weather\commands;


use app\modules\weather\interfaces\IGeoPoint;
use app\modules\weather\interfaces\IGeoPointsRepository;
use app\modules\weather\services\WeatherService;
use yii\console\Controller;

class WeatherUploadController extends Controller
{
    public $geoPointsRepository;
    public $weatherApiService;

    public function __construct($id, $module, IGeoPointsRepository $geoPointsRepository, WeatherService $weatherService)
    {
        parent::__construct($id, $module, []);

        $this->geoPointsRepository = $geoPointsRepository;
        $this->weatherApiService = $weatherService;
    }

    public function actionIndex()
    {
        /** @var IGeoPoint[] $points */
        $points = $this->geoPointsRepository->getAllPoints();

        foreach ($points as $point) {
            $this->weatherApiService->refresh($point);
            sleep(1);
        }
    }
}