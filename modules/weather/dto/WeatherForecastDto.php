<?php

namespace app\modules\weather\dto;


class WeatherForecastDto
{
    public $id;
    public $datetime;
    public $foreignId;
    public $lon;
    public $lat;
    public $temp_min;
    public $temp_avg;
    public $temp_max;
}