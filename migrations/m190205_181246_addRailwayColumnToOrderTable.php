<?php

use app\models\db\Order;
use yii\db\Migration;

/**
 * Class m190205_181246_addRailwayColumnToOrderTable
 */
class m190205_181246_addRailwayColumnToOrderTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'railway_id', $this->integer()->null()->after('meter_id'));

        $this->createIndex(
            'order_railway_id',
            Order::tableName(),
            'railway_id'
        );

        $this->addForeignKey(
            'fk_order_railway_id',
            Order::tableName(),
            'railway_id',
            'railway',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_order_railway_id', Order::tableName());

        $this->dropIndex('order_railway_id', Order::tableName());

        $this->dropColumn(Order::tableName(), 'railway_id');
    }
}
